# #playlist

Documentation and other documents can be found in the doc folder

## Dev Setup

### Code Style

This project uses the [Google Java Style](https://google-styleguide.googlecode.com/svn/trunk/javaguide.html) for code formatting.

An Eclipse definition can be downloaded on [Google Code](https://google-styleguide.googlecode.com/svn/trunk/eclipse-java-google-style.xml). This can then be imported in Eclipse through Window > Preferences > Java > Code Style > Formatter. Once the profile is imported, set the active to *GoogleStyle*.

Next, click "Edit" and go to the "Line Wrapping" tab. At the bottom of the window, set the "Line Wrapping Policy" to  "Do not wrap"."

To automatically format the code when a file is saved (recommended), follow [these instructions](http://stackoverflow.com/a/234625/132509):

> Under Preferences, choose Java --> Editor --> Save Actions. Check the "Perform the selected actions on save", and check the "Format source code" box.


### Maven Setup

Install the [m2e](http://wiki.eclipse.org/Maven_Integration) Eclipse plugin.
