package edu.purdue.cs408.team9;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.UIManager;

import org.jdesktop.jxlayer.JXLayer;
import org.jdesktop.swingx.JXMultiSplitPane;

import edu.purdue.cs408.team9.database.DatabaseManager;
import edu.purdue.cs408.team9.database.models.Source;
import edu.purdue.cs408.team9.database.models.Tag;
import edu.purdue.cs408.team9.database.models.Source.SourceTypes;
import edu.purdue.cs408.team9.export.ExportService;
import edu.purdue.cs408.team9.io.DirectoryUtils;
import edu.purdue.cs408.team9.io.FileCrawlWorker;
import edu.purdue.cs408.team9.loading.LoadingLayerUI;
import edu.purdue.cs408.team9.loading.LoadingService;
import edu.purdue.cs408.team9.ui.Filter;
import edu.purdue.cs408.team9.ui.SearchBar;
import edu.purdue.cs408.team9.ui.TripleSplitPaneModel;
import edu.purdue.cs408.team9.ui.filterpanel.FilterPanel;
import edu.purdue.cs408.team9.ui.songpanel.SongPanel;
import edu.purdue.cs408.team9.ui.tagpanel.TagPanel;
import edu.purdue.cs408.team9.utils.LogUtils;

/**
 * This is the main window
 */
public class HashtagPlaylistApp extends JFrame {
  private static final long serialVersionUID = -6987791206231775970L;

  final File databaseFile = new File(DirectoryUtils.getUserConfigurationDirectory() + "db.sqlite");

  public DatabaseManager db;
  private ExportService exportService;
  private SearchBar searchBar;
  private FilterPanel filterPanel;
  private SongPanel songPanel;
  private TagPanel tagPanel;
  private LoadingService loadingService;
  private JXLayer<JPanel> loadingLayer;
  private LoadingLayerUI loadingLayerUI;
  private JPanel container; /* wrapper for all content */
  private JToolBar toolBar;

  public HashtagPlaylistApp() {
    super("#Playlist");
    try {
      db = new DatabaseManager(databaseFile);
    } catch (SQLException | IOException e) {
      e.printStackTrace();
      JOptionPane.showMessageDialog(null,
          "An error occurred with with the data manager:\n" + e.getMessage(), "#Playlist Error",
          JOptionPane.ERROR_MESSAGE);
      System.exit(1);
    }

    loadingService = new LoadingService();
    loadingLayerUI = new LoadingLayerUI();
    loadingService.addIndicatorCallback(isLoading -> loadingIndicatorChanged(isLoading));
    container = new JPanel();
    initComponents();
    
    exportService = new ExportService(filterPanel);
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setVisible(true);
    setSize(780, 502);
    setLocationRelativeTo(null);
  }

  /**
   * Initialize all ui components
   */
  private void initComponents() {
    this.setJMenuBar(createJMenuBar());

    // Top tool bar
    toolBar = new JToolBar(JToolBar.HORIZONTAL);
    toolBar.setBorder(UIManager.getBorder("TableHeader.cellBorder"));
    toolBar.setBackground(Color.LIGHT_GRAY);
    toolBar.setFloatable(false);

    JButton exportButton = new JButton("Export Playlist");
    exportButton.setOpaque(false);
    exportButton.setFocusable(false);
    exportButton
        .setToolTipText("Create a playlist file from the songs filtered by the Applied Filters. (but not the search bar)");
    exportButton.addActionListener(event -> exportButtonPressed(event));
    toolBar.add(exportButton);

    toolBar.addSeparator();

    searchBar = new SearchBar(this);
    searchBar.setToolTipText("");
    searchBar.setMaximumSize(new Dimension(32767, 20));
    toolBar.add(searchBar);

    toolBar.addSeparator();

    JXMultiSplitPane splitPane1 = new JXMultiSplitPane();

    GroupLayout groupLayout = new GroupLayout(container);
    groupLayout.setHorizontalGroup(
    	groupLayout.createParallelGroup(Alignment.LEADING)
    		.addComponent(toolBar, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
    		.addComponent(splitPane1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
    );
    groupLayout.setVerticalGroup(
    	groupLayout.createParallelGroup(Alignment.LEADING)
    		.addGroup(groupLayout.createSequentialGroup()
    			.addComponent(toolBar, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
    			.addGap(1)
    			.addComponent(splitPane1, GroupLayout.DEFAULT_SIZE, 417, Short.MAX_VALUE))
    );

    splitPane1.setModel(new TripleSplitPaneModel());

    filterPanel = new FilterPanel(db, this);
    splitPane1.add(filterPanel, TripleSplitPaneModel.P1);

    songPanel = new SongPanel(db, this);
    splitPane1.add(songPanel, TripleSplitPaneModel.P2);

    tagPanel = new TagPanel(db, this);
    tagPanel.setPreferredSize(new Dimension(150, 283));
    splitPane1.add(tagPanel, TripleSplitPaneModel.P3);

    JButton importFolderButton = new JButton("Import Folder");
    importFolderButton.setOpaque(false);
    importFolderButton.setFocusable(false);
    importFolderButton.setToolTipText("Import songs inside a folder.");
    importFolderButton.addActionListener(event -> importFolderButtonPressed(event));

    JButton importFileButton = new JButton("Import File");
    importFileButton.setOpaque(false);
    importFileButton.setFocusable(false);
    importFileButton.setToolTipText("Import a single song file.");
    importFileButton.addActionListener(event -> importFileButtonPressed(event));
    toolBar.add(importFileButton);
    toolBar.addSeparator();
    toolBar.add(importFolderButton);
    toolBar.addSeparator();

    JButton btnRemoveImport = new JButton("Remove Import");
    btnRemoveImport.setOpaque(false);
    btnRemoveImport.addActionListener(event -> removeImportButtonPressed(event));
    toolBar.add(btnRemoveImport);

    container.setLayout(groupLayout);

    loadingLayer = new JXLayer<JPanel>(container);
    loadingLayer.setUI(loadingLayerUI);
    this.getContentPane().add(loadingLayer);


  }

  /**
   * Create the menubar and attach all the actions
   * 
   * @return
   */
  private JMenuBar createJMenuBar() {
    JMenuBar menuBar = new JMenuBar();
    JMenu fileMenu = new JMenu("File");
    fileMenu.setMnemonic(KeyEvent.VK_F);

    menuBar.add(fileMenu);

    JMenuItem quitItem = new JMenuItem("Quit");
    JMenuItem clearItem = new JMenuItem("Clear Database");

    quitItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, ActionEvent.ALT_MASK));

    quitItem.addActionListener(event -> quit(event));
    clearItem.addActionListener(event -> clearDatabase(event));

    fileMenu.add(clearItem);
    fileMenu.addSeparator();
    fileMenu.add(quitItem);

    JMenu mnHelp = new JMenu("Help");
    menuBar.add(mnHelp);

    JMenuItem mntmAbout = new JMenuItem("About");
    mnHelp.add(mntmAbout);
    mntmAbout.addActionListener(event -> about(event));

    JMenuItem mntmHowToUse = new JMenuItem("How To Use");
    mnHelp.add(mntmHowToUse);
    mntmHowToUse.addActionListener(event -> howToUse(event));

    return menuBar;
  }

  public void removeImportButtonPressed(ActionEvent event) {
    try {
      List<Source> sources = db.getDataFetcher().getAllSources();
      Source s =
          (Source) JOptionPane.showInputDialog(this, "Choose an import to remove:",
              "Remove Import", JOptionPane.PLAIN_MESSAGE, null, sources.toArray(), null);
      if (s != null) {
        db.getDataCreator().removeSource(s);
        songPanel.update();
      }
    } catch (SQLException e) {
      showErrorDialog(e);
    }
  }

  public void about(ActionEvent event) {
    String msg = "Authors:\nDaniel Alonso, Alec Gorge, Nick Stanish, Ian Moran, Chen Gong";
    JOptionPane.showMessageDialog(this, msg, "About #Playlist", JOptionPane.PLAIN_MESSAGE,
        new ImageIcon(Filter.class.getResource("/icons/Tag.png")));
  }

  public void howToUse(ActionEvent event) {
    String msg = "This program is intended to make organizing your personal music easier.";
    msg +=
        "\nThe main idea is that you can hashtag each song with many different tags and filter your music according to those tags.";
    msg +=
        "\nThen you can export your filtered music as a playlist, which you can import into media players such as iTunes.";
    msg += "\nTour:";
    msg += "\n-The middle column is where your imported songs are displayed.";
    msg +=
        "\n-The left column is where you can filter the songs displayed by applying filters and tags.";
    msg +=
        "\n-The right column shows what tags each song you selected have. Here you can also add and remove tags from selected songs.";
    msg +=
        "\n-The toolbar above the 3 main columns provides many actions such as: importing songs, exporting playlists, and searching for specific songs by text.";
    msg +=
        "\n-You can hover your mouse over a component to see a tooltip with more information on that component.";
    JOptionPane.showMessageDialog(this, msg, "How To Use #Playlist", JOptionPane.PLAIN_MESSAGE,
        new ImageIcon(Filter.class.getResource("/icons/Tag.png")));
  }

  public void importFileButtonPressed(ActionEvent event) {
    LogUtils.logInfo("[Import] File Button pressed", event.toString());
    (new FileCrawlWorker(this, db.getDataCreator(), SourceTypes.FileSource)).execute();
  }

  public void importFolderButtonPressed(ActionEvent event) {
    LogUtils.logInfo("[Import] Folder Button pressed", event.toString());
    (new FileCrawlWorker(this, db.getDataCreator(), SourceTypes.FolderSource)).execute();
  }

  public void exportButtonPressed(ActionEvent event) {
    LogUtils.logInfo("[Export] Button pressed", event.toString());
    exportService.showDialog(this, loadingService);
  }

  public void quit(ActionEvent event) {
    LogUtils.logInfo("[Quit] Menu item clicked", event.toString());
    System.exit(0);
  }

  public void clearDatabase(ActionEvent event) {
    if (loadingService.isLoading()) {
      JOptionPane.showMessageDialog(this, "Cannot clear database while loading.");
      return;
    }
    int choice =
        JOptionPane
            .showConfirmDialog(
                this,
                "Are you sure?\nThis operation will remove all Songs, Tags, and Saved Filters from the database.\n(Your personal song files will be untouched.)",
                "Clear Database", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
    if (choice == JOptionPane.YES_OPTION) {

      try {
        db.resetDatabase();
        filterPanel.reset();
        songPanel.update();
      } catch (SQLException | IOException e) {
        showErrorDialog(e);
      }
    }
  }

  public FilterPanel getFilterPanel() {
    return filterPanel;
  }

  public SongPanel getSongPanel() {
    return songPanel;
  }

  public SearchBar getSearchBar() {
    return searchBar;
  }

  public TagPanel getTagPanel() {
    return tagPanel;
  }

  public void loadingIndicatorChanged(Boolean isLoading) {
    if (isLoading) {
      loadingLayerUI.start();
    } else {
      loadingLayerUI.stop();
    }
  }

  public void showErrorDialog(Exception e) {
    JOptionPane.showMessageDialog(this, "Error: " + e.toString(), "Error",
        JOptionPane.ERROR_MESSAGE);
    e.printStackTrace();
  }

  public LoadingService getLoadingService() {
    return loadingService;
  }
}
