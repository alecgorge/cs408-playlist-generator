package edu.purdue.cs408.team9.database;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.concurrent.Callable;

import com.j256.ormlite.stmt.DeleteBuilder;

import edu.purdue.cs408.team9.database.models.MediaItem;
import edu.purdue.cs408.team9.database.models.MediaItemTag;
import edu.purdue.cs408.team9.database.models.SavedFilter;
import edu.purdue.cs408.team9.database.models.Source;
import edu.purdue.cs408.team9.database.models.Tag;

/**
 * The data creator is used to perform abstracted create, update and delete operations on the database.
 */
public class DataCreator {
  DatabaseManager manager;

  /**
   * Instantiates a new data creator.
   *
   * @param manager the manager
   */
  public DataCreator(DatabaseManager manager) {
    this.manager = manager;
  }
  
  /**
   * Adds the source.
   *
   * @param src the Source
   * @return true, if successful
   * @throws SQLException the SQL exception
   */
  public boolean addSource(Source src) throws SQLException {
    return getManager().getDaoSource().create(src) == 1;
  }
  
  /**
   * Removes the source. This will also remove all associated MediaItems.
   *
   * @param src the Source
   * @return true, if successful
   * @throws SQLException the SQL exception
   */
  public boolean removeSource(Source src) throws SQLException {
    if(getManager().getDaoSource().delete(src) == 1) {
      DeleteBuilder<MediaItem, Integer> del = getManager().getDaoMediaItem().deleteBuilder();
      del.where().eq("source_id", src.getId());
      
      getManager().getDaoMediaItem().delete(del.prepare());
      return true;
    }
    else {
      return false;
    }
  }
  
  /**
   * Adds the tag.
   *
   * @param src
   * @return true, if successful
   * @throws SQLException the SQL exception
   */
  public boolean addTag(Tag src) throws SQLException {
    return getManager().getDaoTag().create(src) == 1;
  }
  
  /**
   * Removes the tag.
   *
   * @param src
   * @return true, if successful
   * @throws SQLException the SQL exception
   */
  public boolean removeTag(Tag src) throws SQLException {
    if(getManager().getDaoTag().delete(src) == 1) {
      DeleteBuilder<MediaItemTag, Integer> del = getManager().getDaoMediaItemTag().deleteBuilder();
      del.where().eq(MediaItemTag.TAG_COL_NAME, src.getId());
      
      getManager().getDaoMediaItemTag().delete(del.prepare());
      
      for (SavedFilter f : getManager().getDataFetcher().getAllSavedFilters()) {
        boolean dirty = false;
        for (Iterator<Tag> iterator = f.getQuery().getTerms().iterator(); iterator.hasNext();) {
          Tag t = iterator.next();
          if (t.equals(src)) {
            dirty = true;
            iterator.remove();
          }
        }
        
        if(dirty) {
          getManager().getDaoSavedFilter().update(f);
        }
      }

      return true;
    }
    
    return false;
  }
  
  /**
   * Update tag.
   *
   * @param src
   * @return true, if successful
   * @throws SQLException the SQL exception
   */
  public boolean updateTag(Tag src) throws SQLException {
    return getManager().getDaoTag().update(src) == 1;
  }
  
  /**
   * Adds the saved filter.
   *
   * @param src
   * @return true, if successful
   * @throws SQLException the SQL exception
   */
  public boolean addSavedFilter(SavedFilter src) throws SQLException {
    return getManager().getDaoSavedFilter().create(src) == 1;
  }
  
  /**
   * Removes the saved filter.
   *
   * @param src
   * @return true, if successful
   * @throws SQLException the SQL exception
   */
  public boolean removeSavedFilter(SavedFilter src) throws SQLException {
    return getManager().getDaoSavedFilter().delete(src) == 1;
  }
  
  /**
   * Update saved filter.
   *
   * @param src
   * @return true, if successful
   * @throws SQLException the SQL exception
   */
  public boolean updateSavedFilter(SavedFilter src) throws SQLException {
    return getManager().getDaoSavedFilter().update(src) == 1;
  }
  
  /**
   * Remove a media item
   * 
   * @param item
   * @return true, if successful
   * @throws SQLException
   */
  public boolean removeMediaItem(MediaItem item) throws SQLException {
    return getManager().getDaoMediaItem().delete(item) == 1;
  }
  
  /**
   * Adds the media item. You do not need to add the system tags like artist or album.
   *
   * @param item the item
   * @return true, if successful
   * @throws SQLException the SQL exception
   */
  public boolean addMediaItem(MediaItem item) throws SQLException {
    boolean createdMediaItem = getManager().getDaoMediaItem().create(item) == 1;
    
    if(!createdMediaItem) {
      return createdMediaItem;
    }
    
    Tag artistTag = new Tag();
    artistTag.setSystemTag(true);
    artistTag.setSystemTagType(Tag.SystemTagType.ArtistTag);
    artistTag.setName(item.getMetaArtist());
    
    Tag albumTag = new Tag();
    albumTag.setSystemTag(true);
    albumTag.setSystemTagType(Tag.SystemTagType.AlbumTag);
    albumTag.setName(item.getMetaAlbum());

    for(Tag t : new Tag[] { artistTag, albumTag }) {
      if(!createTagIfNotExistsAndAddToMediaItem(t, item)) {
        return false;
      }
    }

    for(Tag t : item.getTransientTags()) {
      if(!createTagIfNotExistsAndAddToMediaItem(t, item)) {
        return false;
      }
    }
    
    return true;
  }
  
  boolean createTagIfNotExistsAndAddToMediaItem(Tag t, MediaItem item) throws SQLException {
    return addTagIfNotExistsInDatabase(t) && addTagToMediaItem(item, t); 
  }
  
  /**
   * Adds the media item if it doesn't already exist in the database as determined by CRC32. The system tags are generated automatically.
   *
   * @param item the item
   * @return true, if successful
   * @throws SQLException the SQL exception
   */
  public boolean addMediaItemIfNotExistsInDatabase(MediaItem item) throws SQLException {
    if(!getManager().getDataFetcher().doesMediaItemExistInDatabase(item)) {
      return addMediaItem(item);
    }
    
    return true;
  }
  
  /**
   * Adds the tag if it doesn't already exist in the database as determined by the name and tag type
   *
   * @param item the item
   * @return true, if successful
   * @throws SQLException the SQL exception
   */
  public boolean addTagIfNotExistsInDatabase(Tag item) throws SQLException {
    if(!getManager().getDataFetcher().doesTagExistInDatabase(item)) {
      return addTag(item);
    }
    
    return true;
  }
  
  /**
   * Adds the tag to media item.
   *
   * @param item the item
   * @param tag the tag
   * @return true, if successful
   * @throws SQLException the SQL exception
   */
  public  boolean addTagToMediaItem(MediaItem item, Tag tag) throws SQLException {
    MediaItemTag mit = new MediaItemTag(item, tag);
    return getManager().getDaoMediaItemTag().create(mit) == 1;
  }
  
  /**
   * Removes the tag from media item.
   *
   * @param item the item
   * @param tag the tag
   * @return true, if successful
   * @throws SQLException the SQL exception
   */
  public boolean removeTagFromMediaItem(MediaItem item, Tag tag) throws SQLException {
    DeleteBuilder<MediaItemTag, Integer> mitQb = getManager().getDaoMediaItemTag().deleteBuilder();
    mitQb.where().eq(MediaItemTag.MEDIAITEM_COL_NAME, item.getId()).and().eq(MediaItemTag.TAG_COL_NAME, tag.getId());
    
    return getManager().getDaoMediaItemTag().delete(mitQb.prepare()) != 0;
  }
  
  /**
   * Perform batch operations. This is useful to do bulk inserts. This is NOT a transaction and is NOT atomic. 
   *
   * @param All operations run in Callable instance will not take effect until completion of the method.
   * @throws Exception the exception
   */
  public void performBatchOperations(Callable<Void> cb) throws Exception {
    // we can use any DAO to batch operate
    getManager().getDaoSource().callBatchTasks(cb);
  }
  
  /**
   * Gets the manager.
   *
   * @return the manager
   */
  DatabaseManager getManager() {
    return manager;
  }
}
