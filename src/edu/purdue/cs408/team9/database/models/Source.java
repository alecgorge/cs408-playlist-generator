package edu.purdue.cs408.team9.database.models;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "sources")
public class Source {

  public enum SourceTypes {
    FileSource, FolderSource, iTunesLibrarySource
  }

  @DatabaseField(generatedId = true)
  int id;

  @DatabaseField
  SourceTypes type;

  @DatabaseField(dataType = DataType.LONG_STRING)
  String path;

  public Source() {

  }

  public SourceTypes getType() {
    return type;
  }

  public void setType(SourceTypes type) {
    this.type = type;
  }

  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public int getId() {
    return id;
  }

  public String toString(){
	  return path;
  }
}
