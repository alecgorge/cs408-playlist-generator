package edu.purdue.cs408.team9.database.models;

import java.util.ArrayList;
import java.util.List;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class MediaItem {

  @DatabaseField(generatedId = true)
  int id;
  
  @DatabaseField
  int trackNumber;

  @DatabaseField
  String metaTitle;

  @DatabaseField
  String metaArtist;

  @DatabaseField
  String metaAlbum;
  
  @DatabaseField
  long metaDuration;

  @DatabaseField(dataType = DataType.LONG_STRING)
  String path;
  
  @DatabaseField(index = true)
  long hash;

  @DatabaseField(foreign = true)
  Source source;
  
  @ForeignCollectionField(eager = true)
  transient ForeignCollection<MediaItemTag> tags;
  
  transient List<Tag> transientTags = new ArrayList<Tag>();

  public MediaItem() {

  }

  public int getTrackNumber() {
    return trackNumber;
  }

  public void setTrackNumber(int trackNumber) {
    this.trackNumber = trackNumber;
  }

  public String getMetaTitle() {
    return metaTitle;
  }

  public void setMetaTitle(String metaTitle) {
    this.metaTitle = metaTitle;
  }

  public long getMetaDuration() {
    return metaDuration;
  }

  public void setMetaDuration(long metaDuration) {
    this.metaDuration = metaDuration;
  }

  public String getMetaArtist() {
    return metaArtist;
  }

  public void setMetaArtist(String metaArtist) {
    this.metaArtist = metaArtist;
  }

  public String getMetaAlbum() {
    return metaAlbum;
  }

  public void setMetaAlbum(String metaAlbum) {
    this.metaAlbum = metaAlbum;
  }

  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public Source getSource() {
    return source;
  }

  public void setSource(Source source) {
    this.source = source;
  }

  public int getId() {
    return id;
  }
  
  public ForeignCollection<MediaItemTag> getTags() {
    return tags;
  }
  
  /**
   * Transient tags are used for initial addition of tags.
   * 
   * getTags cannot be used because the MediaItem does not have an ID yet. This allows MediaItem to encapsulate tags at all points in its lifecycle.
   * 
   * @return
   */
  public List<Tag> getTransientTags() {
    return transientTags;
  }
  
  public void setTransientTags(List<Tag> newTags){
	  transientTags = newTags;
  }
  
  /**
   * Sets the hash. It should be an 8 character CRC32 hash string
   *
   * @param crc32hash the new hash
   */
  public void setHash(long crc32hash) {
    this.hash = crc32hash;
  }
  
  /**
   * Gets the CRC32 hash.
   *
   * @return the hash
   */
  public long getHash() {
    return hash;
  }
}
