package edu.purdue.cs408.team9.database.models;

import java.io.Serializable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

// TODO: Auto-generated Javadoc
/**
 * The Class Tag.
 */
@DatabaseTable
public class Tag implements Serializable {
  
  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 3425285852230066708L;

  /**
   * The Enum SystemTagType.
   */
  public enum SystemTagType {
    /** The Artist tag. */
    ArtistTag, 
    
    /** The Album tag. */
    AlbumTag
  }

  /** The id. */
  @DatabaseField(generatedId = true)
  int id;

  /** The name. */
  @DatabaseField
  String name;

  /** The system tag. */
  @DatabaseField
  boolean systemTag;

  /** The system tag type. */
  @DatabaseField
  SystemTagType systemTagType;

  /**
   * Instantiates a new tag.
   */
  public Tag() {

  }

  /**
   * Gets the name.
   *
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * Sets the name.
   *
   * @param name the new name
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Checks if is system tag.
   *
   * @return true, if is system tag
   */
  public boolean isSystemTag() {
    return systemTag;
  }

  /**
   * Sets the system tag.
   *
   * @param systemTag the new system tag
   */
  public void setSystemTag(boolean systemTag) {
    this.systemTag = systemTag;
  }

  /**
   * Gets the system tag type.
   *
   * @return the system tag type
   */
  public SystemTagType getSystemTagType() {
    return systemTagType;
  }

  /**
   * Sets the system tag type.
   *
   * @param systemTagType the new system tag type
   */
  public void setSystemTagType(SystemTagType systemTagType) {
    this.systemTagType = systemTagType;
  }

  /**
   * Gets the id.
   *
   * @return the id
   */
  public int getId() {
    return id;
  }

  public void setId(int i) {
    id = i;
  }
  
  @Override
  public boolean equals(Object obj) {
    if (obj instanceof Tag) {
      Tag t = (Tag) obj;
      return t.isSystemTag() == isSystemTag() && t.getName().equals(getName()) && t.getSystemTagType() == getSystemTagType();      
    }
    return super.equals(obj);
  }
  
  public String toString()
  {
	  return name;
  }
}
