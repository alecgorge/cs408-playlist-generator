package edu.purdue.cs408.team9.database.models;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import edu.purdue.cs408.team9.database.Query;

// TODO: Auto-generated Javadoc
/**
 * The Class SavedFilter.
 */
@DatabaseTable
public class SavedFilter {
  
  @DatabaseField(generatedId = true)
  int id;

  @DatabaseField
  String name;

  @DatabaseField(dataType = DataType.SERIALIZABLE)
  Query query;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Query getQuery() {
    return query;
  }

  public void setQuery(Query query) {
    this.query = query;
  }

  public int getId() {
    return id;
  }
}
