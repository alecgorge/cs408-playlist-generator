package edu.purdue.cs408.team9.database.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName="mediaitem_tag")
public class MediaItemTag {  
  public static final String MEDIAITEM_COL_NAME = "mediaitem_id";
  public static final String TAG_COL_NAME = "tag_id";

  @DatabaseField(generatedId = true)
  int id;

  @DatabaseField(foreign = true, columnName=MEDIAITEM_COL_NAME)
  MediaItem mediaItem;

  @DatabaseField(foreign = true, columnName=TAG_COL_NAME, foreignAutoRefresh = true)
  Tag tag;

  MediaItemTag() {

  }

  public MediaItemTag(MediaItem mediaItem, Tag tag) {
    this.mediaItem = mediaItem;
    this.tag = tag;
  }
  
  public MediaItem getMediaItem() {
    return mediaItem;
  }
  
  public Tag getTag() {
    return tag;
  }
}
