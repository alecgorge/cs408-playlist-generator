package edu.purdue.cs408.team9.database;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.table.TableUtils;

import edu.purdue.cs408.team9.database.models.MediaItem;
import edu.purdue.cs408.team9.database.models.MediaItemTag;
import edu.purdue.cs408.team9.database.models.SavedFilter;
import edu.purdue.cs408.team9.database.models.Source;
import edu.purdue.cs408.team9.database.models.Tag;
import edu.purdue.cs408.team9.utils.LogUtils;

/**
 * This class opens the connection to the SQLite3 database and manages table creation and the DAOs
 * for the different tables.
 */
public class DatabaseManager {

  /** The location of the database file */
  File databaseFile = null;

  JdbcConnectionSource databaseConnectionSource = null;
  DataFetcher dataFetcher = new DataFetcher(this);
  DataCreator dataCreator = new DataCreator(this);
  Dao<Source, Integer> daoSource = null;
  Dao<MediaItem, Integer> daoMediaItem = null;
  Dao<Tag, Integer> daoTag = null;
  Dao<MediaItemTag, Integer> daoMediaItemTag = null;
  Dao<SavedFilter, Integer> daoSavedFilter = null;

  /**
   * Instantiates a new database manager.
   *
   * @param databaseFile The path to store the db. null for :memory:
   * @throws SQLException Indicates an issue opening the database or doing initial table setup
   * @throws IOException If the db file can't be written to
   */
  public DatabaseManager(File databaseFile) throws SQLException, IOException {
    this.databaseFile = databaseFile;

    setupDatabase();
  }

  void setupDatabase() throws SQLException, IOException {
    connect();
    buildDaos(getDatabaseConnectionSource());
    createTables(getDatabaseConnectionSource());
  }

  public void resetDatabase() throws SQLException, IOException {
    close();

    databaseFile.delete();
    databaseFile.createNewFile();

    setupDatabase();
  }

  JdbcConnectionSource connect() throws SQLException, IOException {
    if (databaseConnectionSource == null) {
      String databasePath = null;
      if (databaseFile != null) {
        if (databaseFile.getParentFile() != null) {
          databaseFile.getParentFile().mkdirs();
        }
        databaseFile.createNewFile();
        databasePath = databaseFile.getAbsolutePath();
      } else {
        databasePath = ":memory:";
      }

      LogUtils.logInfo(databasePath);

      databaseConnectionSource = new JdbcConnectionSource("jdbc:sqlite:" + databasePath);
    }

    return databaseConnectionSource;
  }

  void buildDaos(JdbcConnectionSource conn) throws SQLException {
    daoSource = DaoManager.createDao(conn, Source.class);
    daoMediaItem = DaoManager.createDao(conn, MediaItem.class);
    daoTag = DaoManager.createDao(conn, Tag.class);
    daoMediaItemTag = DaoManager.createDao(conn, MediaItemTag.class);
    daoSavedFilter = DaoManager.createDao(conn, SavedFilter.class);
  }

  void createTables(JdbcConnectionSource conn) throws SQLException {
    TableUtils.createTableIfNotExists(conn, Source.class);
    TableUtils.createTableIfNotExists(conn, MediaItem.class);
    TableUtils.createTableIfNotExists(conn, Tag.class);
    TableUtils.createTableIfNotExists(conn, MediaItemTag.class);
    TableUtils.createTableIfNotExists(conn, SavedFilter.class);
  }

  /**
   * Gets the raw JDBC database connection source.
   *
   * @return the database connection source
   */
  JdbcConnectionSource getDatabaseConnectionSource() {
    return databaseConnectionSource;
  }

  public Dao<Source, Integer> getDaoSource() {
    return daoSource;
  }

  public Dao<MediaItem, Integer> getDaoMediaItem() {
    return daoMediaItem;
  }

  public Dao<Tag, Integer> getDaoTag() {
    return daoTag;
  }

  public Dao<MediaItemTag, Integer> getDaoMediaItemTag() {
    return daoMediaItemTag;
  }

  public Dao<SavedFilter, Integer> getDaoSavedFilter() {
    return daoSavedFilter;
  }

  /**
   * Gets the data fetcher. The data fetcher is an abstraction for read operations and handles most
   * use cases.
   *
   * @return the data fetcher
   */
  public DataFetcher getDataFetcher() {
    return dataFetcher;
  }

  /**
   * Gets the data creator. The data creator is used to perform abstracted create, update and delete
   * operations on the database.
   *
   * @return the data creator
   */
  public DataCreator getDataCreator() {
    return dataCreator;
  }

  /**
   * Close the connection to the database and free up resources.
   * 
   * @throws SQLException
   */
  public void close() throws SQLException {
    if (databaseConnectionSource != null && databaseConnectionSource.isOpen()) {
      databaseConnectionSource.close();
    }
  }
}
