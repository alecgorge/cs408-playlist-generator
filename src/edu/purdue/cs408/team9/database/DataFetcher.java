package edu.purdue.cs408.team9.database;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import com.j256.ormlite.stmt.ColumnArg;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.SelectArg;
import com.j256.ormlite.stmt.Where;

import edu.purdue.cs408.team9.database.models.MediaItem;
import edu.purdue.cs408.team9.database.models.MediaItemTag;
import edu.purdue.cs408.team9.database.models.SavedFilter;
import edu.purdue.cs408.team9.database.models.Source;
import edu.purdue.cs408.team9.database.models.Tag;

/**
 * An abstraction for read operations and handles most use cases.
 */
public class DataFetcher {
  DatabaseManager manager;

  /**
   * Instantiates a new data fetcher.
   *
   * @param manager the manager
   */
  public DataFetcher(DatabaseManager manager) {
    this.manager = manager;
  }

  /** The sources query. */
  PreparedQuery<Source> sourcesQuery = null;
  
  /** The all user tags query. */
  PreparedQuery<Tag> allUserTagsQuery = null;
  
  /** The all artist tags query. */
  PreparedQuery<Tag> allArtistTagsQuery = null;
  
  /** The all album tags query. */
  PreparedQuery<Tag> allAlbumTagsQuery = null;
  
  /** The all saved filters query. */
  PreparedQuery<SavedFilter> allSavedFiltersQuery = null;
  
  /** The filtered media items query. */
  PreparedQuery<MediaItem> filteredMediaItemsQuery = null;
  
  /** The media item existence query. */
  PreparedQuery<MediaItem> mediaItemExistenceQuery = null;

  /** The tag existence query. */
  PreparedQuery<Tag> tagExistenceQuery = null;

  /** The media item existence arg. */
  SelectArg mediaItemExistenceArg = new SelectArg();

  /** The placeholder in the prepared query for the tag type */
  SelectArg tagExistenceTypeArg = new SelectArg();

  /** The placeholder in the prepared query for the tag title */
  SelectArg tagExistenceTitleArg = new SelectArg();
  
  SelectArg tagExistenceEnumArg = new SelectArg();

  /**
   * Gets the all sources.
   *
   * @return the all sources
   * @throws SQLException the SQL exception
   */
  public List<Source> getAllSources() throws SQLException {
    return getManager().getDaoSource().query(getSourcesQuery());
  }

  /**
   * Gets the all user tags.
   *
   * @return the all user tags
   * @throws SQLException the SQL exception
   */
  public List<Tag> getAllUserTags() throws SQLException {
    return getManager().getDaoTag().query(getAllUserTagsQuery());
  }

  /**
   * Gets the all artist tags.
   *
   * @return the all artist tags
   * @throws SQLException the SQL exception
   */
  public List<Tag> getAllArtistTags() throws SQLException {
    return getManager().getDaoTag().query(getAllArtistTagsQuery());
  }

  /**
   * Gets the all album tags.
   *
   * @return the all album tags
   * @throws SQLException the SQL exception
   */
  public List<Tag> getAllAlbumTags() throws SQLException {
    return getManager().getDaoTag().query(getAllAlbumTagsQuery());
  }

  /**
   * Gets the all saved filters.
   *
   * @return the all the saved filters
   * @throws SQLException the SQL exception
   */
  public List<SavedFilter> getAllSavedFilters() throws SQLException {
    return getManager().getDaoSavedFilter().query(getAllSavedFiltersQuery());
  }

  /**
   * Gets the media items based on the filter represented in the Query
   *
   * @param the query to use as the filter. All filters in the UI should be representable in Query 
   * @return the media items for query
   * @throws SQLException the SQL exception
   */
  public List<MediaItem> getMediaItemsForQuery(Query query) throws SQLException {
    QueryBuilder<MediaItem, Integer> mediaItemQb = getManager().getDaoMediaItem().queryBuilder().distinct();
    QueryBuilder<MediaItemTag, Integer> joinQb = getManager().getDaoMediaItemTag().queryBuilder();

    if (query.getStringFilter() != null) {
      Where<MediaItem, Integer> where = mediaItemQb.where();

      where.like("metaArtist", new SelectArg("%" + query.getStringFilter().replaceAll("%", "\\%").replaceAll("_", "\\_") + "%"));
      where.like("metaAlbum", new SelectArg("%" + query.getStringFilter().replaceAll("%", "\\%").replaceAll("_", "\\_") + "%"));
      where.like("metaTitle", new SelectArg("%" + query.getStringFilter().replaceAll("%", "\\%").replaceAll("_", "\\_") + "%"));

      // reverse polish notation. creates an or between the previous 3 conditions
      where.or(3);
    }

    if(query.getTerms().size() > 0) {
      Where<MediaItemTag, Integer> qbwhere = joinQb.where();
      
      qbwhere.eq(MediaItemTag.MEDIAITEM_COL_NAME, new ColumnArg("mediaitem", "id"));
      qbwhere.and().in(MediaItemTag.TAG_COL_NAME, query.getTerms().stream().filter(t -> t != null).map(t -> t.getId()).collect(Collectors.toList()));
  
      mediaItemQb.join(joinQb);
      mediaItemQb.groupBy("id");
      
      if (query.getTermJoinType() == Query.TermJoinType.Intersection) {
        mediaItemQb.having("COUNT(DISTINCT mediaitem_tag." + MediaItemTag.TAG_COL_NAME + ") = " + query.getTerms().size());
      }
    }

    return getManager().getDaoMediaItem().query(mediaItemQb.prepare());
  }
  
  public Tag tagForName(String name) throws SQLException {
    return tagForDetails(name, false, Tag.SystemTagType.AlbumTag);
  }
  
  public Tag systemTagForName(String name, Tag.SystemTagType tagType) throws SQLException {
    return tagForDetails(name, true, tagType);
  }
  
  public Tag tagForDetails(String name, boolean system, Tag.SystemTagType tagType) throws SQLException {
    QueryBuilder<Tag, Integer> qb = getManager().getDaoTag().queryBuilder();
    
    Where<Tag, Integer> where = qb.where();
    if(system) {
      where.eq("systemTag", system).and().eq("name", name).and().eq("systemTagType", tagType);      
    }
    else {
      where.eq("systemTag", system).and().eq("name", name);
    }
    qb.limit(1L);
    
    return getManager().getDaoTag().queryForFirst(qb.prepare());
  }

  /**
   * Checks if a media item exists based on the hash value (CRC32)
   *
   * @param The transient MediaItem to look for in the database
   * @return true, if it does exist
   * @throws SQLException the SQL exception
   */
  public boolean doesMediaItemExistInDatabase(MediaItem item) throws SQLException {
    return getManager().getDaoMediaItem().query(getMediaItemExistanceQuery(item)).size() > 0;
  }

  /**
   * Refresh media item.
   *
   * @param item the item
   * @throws SQLException the SQL exception
   */
  public void refreshMediaItem(MediaItem item) throws SQLException {
    getManager().getDaoMediaItem().refresh(item);
  }
  
  /**
   * Refresh media items.
   *
   * @param items the items
   * @throws SQLException the SQL exception
   */
  public void refreshMediaItems(Collection<MediaItem> items) throws SQLException {
    for(MediaItem i : items) {
      refreshMediaItem(i);
    }
  }
  
  /**
   * Checks if a media item exists based on the hash value (CRC32)
   *
   * @param The transient MediaItem to look for in the database
   * @return true, if it does exist
   * @throws SQLException the SQL exception
   */
  public boolean doesTagExistInDatabase(Tag item) throws SQLException {
    Tag tagdb = getManager().getDaoTag().queryForFirst(getTagExistanceQuery(item));
    if(tagdb != null) {
      item.setId(tagdb.getId());
      
      return true;
    }
    
    return false;
  }

  DatabaseManager getManager() {
    return manager;
  }

  PreparedQuery<MediaItem> getMediaItemExistanceQuery(MediaItem item) throws SQLException {
    if(mediaItemExistenceQuery == null) {
      QueryBuilder<MediaItem, Integer> qb = getManager().getDaoMediaItem().queryBuilder();
      
      qb.where().eq("hash", mediaItemExistenceArg);
      qb.limit(1L);
      
      mediaItemExistenceQuery = qb.prepare();
    }
    
    mediaItemExistenceArg.setValue(item.getHash());
    
    return mediaItemExistenceQuery;
  }
  
  PreparedQuery<Tag> getTagExistanceQuery(Tag item) throws SQLException {
    if(tagExistenceQuery == null) {
      QueryBuilder<Tag, Integer> qb = getManager().getDaoTag().queryBuilder();
      
      qb.where().eq("systemTag", tagExistenceTypeArg).and().eq("name", tagExistenceTitleArg).and().rawComparison("systemTagType", "IS", tagExistenceEnumArg);
      qb.limit(1L);
      
      tagExistenceQuery = qb.prepare();
    }
    
    tagExistenceTitleArg.setValue(item.getName());
    tagExistenceTypeArg.setValue(item.isSystemTag());
    tagExistenceEnumArg.setValue(item.getSystemTagType());
    
    return tagExistenceQuery;
  }
  
  PreparedQuery<Source> getSourcesQuery() throws SQLException {
    if (sourcesQuery == null) {
      QueryBuilder<Source, Integer> sourceQb = getManager().getDaoSource().queryBuilder();
      sourceQb.orderBy("id", true);

      sourcesQuery = sourceQb.prepare();
    }

    return sourcesQuery;
  }

  PreparedQuery<SavedFilter> getAllSavedFiltersQuery() throws SQLException {
    if (allSavedFiltersQuery == null) {
      QueryBuilder<SavedFilter, Integer> sfQb = getManager().getDaoSavedFilter().queryBuilder();
      sfQb.orderBy("id", true);

      allSavedFiltersQuery = sfQb.prepare();
    }

    return allSavedFiltersQuery;
  }

  PreparedQuery<Tag> getAllUserTagsQuery() throws SQLException {
    if (allUserTagsQuery == null) {
      QueryBuilder<Tag, Integer> tagQb = getManager().getDaoTag().queryBuilder();
      tagQb.where().eq("systemTag", false);
      tagQb.orderBy("name", true);

      allUserTagsQuery = tagQb.prepare();
    }

    return allUserTagsQuery;
  }

  PreparedQuery<Tag> getAllArtistTagsQuery() throws SQLException {
    if (allArtistTagsQuery == null) {
      QueryBuilder<Tag, Integer> tagQb = getManager().getDaoTag().queryBuilder();
      tagQb.where().eq("systemTag", true).and().eq("systemTagType", Tag.SystemTagType.ArtistTag);
      tagQb.orderBy("name", true);

      allArtistTagsQuery = tagQb.prepare();
    }

    return allArtistTagsQuery;
  }

  PreparedQuery<Tag> getAllAlbumTagsQuery() throws SQLException {
    if (allAlbumTagsQuery == null) {
      QueryBuilder<Tag, Integer> tagQb = getManager().getDaoTag().queryBuilder();
      tagQb.where().eq("systemTag", true).and().eq("systemTagType", Tag.SystemTagType.AlbumTag);
      tagQb.orderBy("name", true);

      allAlbumTagsQuery = tagQb.prepare();
    }

    return allAlbumTagsQuery;
  }
}
