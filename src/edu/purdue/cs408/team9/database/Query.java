package edu.purdue.cs408.team9.database;

import java.io.Serializable;
import java.util.ArrayList;

import edu.purdue.cs408.team9.database.models.Tag;

public class Query implements Serializable {
  private static final long serialVersionUID = -4806255657705983133L;

  public enum TermJoinType {
    Union, 
    Intersection
  }
  
  TermJoinType termJoinType = TermJoinType.Union;  
  ArrayList<Tag> terms = new ArrayList<Tag>();
  String stringFilter = null;
  
  public Query() {
    
  }
  
  public void setTermJoinType(TermJoinType joinType) {
    termJoinType = joinType;
  }
  
  public TermJoinType getTermJoinType() {
    return termJoinType;
  }
  
  public ArrayList<Tag> getTerms() {
    return terms;
  }
  
  public void addTerm(Tag term) {
    terms.add(term);
  }
  
  public void removeAllTerms() {
    terms.clear();
  }
  
  public void setStringFilter(String filter) {
    stringFilter = filter;
  }
  
  public String getStringFilter() {
    return stringFilter;
  }
}
