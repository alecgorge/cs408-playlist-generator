package edu.purdue.cs408.team9.exceptions;

import java.io.IOException;

@SuppressWarnings("serial")
public class FileNotWriteableException extends IOException {
  public FileNotWriteableException() {
    super();
  }
}
