package edu.purdue.cs408.team9.loading;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics2D;

import javax.swing.Timer;

import org.jdesktop.jxlayer.JXLayer;
import org.jdesktop.jxlayer.plaf.ext.LockableUI;

public class LoadingLayerUI extends LockableUI {

  private static final long serialVersionUID = -6127508163684026029L;
  private Indicator indicator;

  private Timer timer = new Timer((int) (1000 / 40.0), event -> repaint());

  public LoadingLayerUI() {
    this.indicator = new Indicator(Color.black, 10, 3);
  }

  private void repaint() {
    this.setDirty(true);

  }

  public void start() {
    setLocked(true);
    timer.start();

  }

  public void stop() {
    timer.stop();
    setLocked(false);
  }

  @Override
  protected void paintLayer(Graphics2D g, JXLayer layer) {
    super.paintLayer(g, layer);
    g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, .5f));
    g.setColor(Color.LIGHT_GRAY);
    g.fillRect(0, 0, layer.getWidth(), layer.getHeight());
    indicator.draw(g, layer);
  }
}
