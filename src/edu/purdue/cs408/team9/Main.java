package edu.purdue.cs408.team9;

/*
 * Issues that aren't really issues:
 * 
 * Importing: Severity 2: Import same file/folder multiple times. Expected Result: Only one source
 * should exist. Add new media items. Update the current source to have those media items. (Alec)
 * Severity 2: Import file inside an already imported folder. Expected Result: Update the folder
 * source with the new media item, don't add the file source. (Alec) Severity 2: Import folder with
 * already imported file inside. - Delete the file source. (Alec)
 * 
 * MediaItems: Severity 3: Database should not allow more than one of the same tag(same type and
 * name) in a Media Item. Severity 3 because this case is accounted for in the ui already. (Alec)
 * Severity 3: Deleting media item deletes metadata filters as well if no media items exist with
 * those filters (Alec)
 */
import com.alee.laf.WebLookAndFeel;

public class Main {
  public static void main(String[] args) {

    WebLookAndFeel.install();
    new HashtagPlaylistApp();
  }

}
