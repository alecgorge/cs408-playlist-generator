package edu.purdue.cs408.team9.io;

import java.io.File;

/**
 * This class holds various useful helper methods while working with file system directories
 */
public class DirectoryUtils {

  /**
   * On Windows this is equivalent to C:\Users\Alec\.#playlist\
   * 
   * On Mac/Linux this is equivalent to ~/.#playlist/
   *
   * @return The path to the #playlist config directory in the user directory (with trailing slash)
   */
  public static String getUserConfigurationDirectory() {
    return System.getProperty("user.home") + File.separator + ".#playlist" + File.separator;
  }

  public static String getHomeDirectory() {
    return System.getProperty("user.home");
  }
}
