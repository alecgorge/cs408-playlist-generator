package edu.purdue.cs408.team9.io;

import java.sql.SQLException;

import javax.swing.JOptionPane;
import javax.swing.SwingWorker;

import edu.purdue.cs408.team9.HashtagPlaylistApp;
import edu.purdue.cs408.team9.database.DataCreator;
import edu.purdue.cs408.team9.database.models.Source.SourceTypes;

public class FileCrawlWorker extends SwingWorker<String, String> {

  HashtagPlaylistApp parent;
  DataCreator dbIn;
  SourceTypes sourceType;

  public FileCrawlWorker(HashtagPlaylistApp parent, DataCreator dbIn, SourceTypes sourceType) {
    this.parent = parent;
    this.dbIn = dbIn;
    this.sourceType = sourceType;
  }

  @Override
  public String doInBackground() {
    parent.getLoadingService().showLoadingIndicator();
    try {
      FileCrawler.selectStartPoint(parent, dbIn, sourceType);
    } catch (SQLException e) {
      e.printStackTrace();
      JOptionPane.showMessageDialog(null,
          "An error occurred with with the data manager:\n" + e.getMessage(), "#Playlist Error",
          JOptionPane.ERROR_MESSAGE);
    }
    return null;
  }

  @Override
  protected void done() {
    parent.getLoadingService().hideLoadingIndicator();
    parent.getFilterPanel().refresh();
    parent.getSongPanel().update();
  }
}
