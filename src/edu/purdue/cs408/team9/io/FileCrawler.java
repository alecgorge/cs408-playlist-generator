
package edu.purdue.cs408.team9.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.CRC32;

import javax.swing.JFileChooser;

import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.exceptions.CannotReadException;
import org.jaudiotagger.audio.exceptions.InvalidAudioFrameException;
import org.jaudiotagger.audio.exceptions.ReadOnlyFileException;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.Tag;
import org.jaudiotagger.tag.TagException;

import edu.purdue.cs408.team9.HashtagPlaylistApp;
import edu.purdue.cs408.team9.database.DataCreator;
import edu.purdue.cs408.team9.database.DatabaseManager;
import edu.purdue.cs408.team9.database.models.MediaItem;
import edu.purdue.cs408.team9.database.models.MediaItemTag;
import edu.purdue.cs408.team9.database.models.Source;
import edu.purdue.cs408.team9.database.models.Source.SourceTypes;
import edu.purdue.cs408.team9.utils.LogUtils;

// FileDiscoveryCrawler - searches the file system to discover .mp3 files

public class FileCrawler {
	static String titleForFilepath(String filepath) {
		String str = (new File(filepath)).getName();
		
		int pos = str.lastIndexOf(".");
		if(pos == -1){
			return str;
		}
		
		return str.substring(0, pos);
	}
	
	public static void runCrawler(String filepath, DataCreator dbIn, Source sourceFile) {
		LogUtils.logInfo("Looking at " + filepath);
		File currentFile = new File(filepath);
		if(currentFile.exists()){
			// Check if the file is a audio file or a directory
			if(filepath.endsWith(".mp3")){
				MediaItem fileInfo = new MediaItem();
				AudioFile f = null;
				try{
					f = AudioFileIO.read(currentFile);
				}
				catch(CannotReadException | IOException | TagException | ReadOnlyFileException
									| InvalidAudioFrameException e){
					LogUtils.logError("Audio file exists but cannot be read!");
					e.printStackTrace();
					return;
				}
				if(f == null){
					// TODO: Throw Error (Should never be reached)
					return;
				}
				Tag tag = f.getTag();
				
				LogUtils.logInfo("Getting tag info for " + filepath);
				
				fileInfo.setMetaAlbum(tag.getFirst(FieldKey.ALBUM));
				fileInfo.setMetaArtist(tag.getFirst(FieldKey.ARTIST));
				
				if(!tag.getFirst(FieldKey.TITLE).equals("")){
					fileInfo.setMetaTitle(tag.getFirst(FieldKey.TITLE));
				}
				else{
					fileInfo.setMetaTitle(titleForFilepath(filepath));
				}
				
				fileInfo.setMetaDuration(f.getAudioHeader().getTrackLength());
				try{
					fileInfo.setTrackNumber(Integer.parseInt(tag.getFirst(FieldKey.TRACK)));
				}
				catch(NumberFormatException e1){
					fileInfo.setTrackNumber(-1);
				}
				try{
					fileInfo.setPath(currentFile.getCanonicalPath());
				}
				catch(IOException e1){
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				fileInfo.setSource(sourceFile);
				
				InputStream fileReader = null;
				try{
					fileReader = new FileInputStream(currentFile);
				}
				catch(FileNotFoundException e1){
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				CRC32 crcMaker = new CRC32();
				byte[] buffer = new byte[1024];
				int bytesRead;
				try{
					while((bytesRead = fileReader.read(buffer)) != -1){
						crcMaker.update(buffer, 0, bytesRead);
					}
				}
				catch(IOException e1){
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				fileInfo.setHash(crcMaker.getValue()); // This is your error checking code
				LogUtils.logInfo("Generated Hash:" + fileInfo.getHash());
				
				LogUtils.logInfo("Adding file to database: " + filepath);
				
				try{
					dbIn.addMediaItemIfNotExistsInDatabase(fileInfo);
				}
				catch(SQLException e){
					LogUtils.logError("Could not add file to database!");
					e.printStackTrace();
				}
				
			}
			else if(currentFile.isDirectory()){
				File[] containedFiles = currentFile.listFiles();
				try{
					for(File subFile: containedFiles){
						String subFilepath = subFile.toString();
						runCrawler(subFilepath, dbIn, sourceFile);
					}
				}
				catch(NullPointerException e){
					// TODO: Send notice to user?
					LogUtils.logError("Issue with reading directory, skipping " + currentFile.toString()
										+ " for now.", e);
				}
			}
			// Do not worry about non-audio/directory files
		}
		else{
			// File does not exist, throw error
		}
	}
	
	// selectStartPoint: Deals with the JFileChooser and starting up the Crawler
	// for manually started searches.
	public static void selectStartPoint(HashtagPlaylistApp parent, DataCreator dbIn,
						SourceTypes sourceType) throws SQLException {
		JFileChooser chooser = new JFileChooser(DirectoryUtils.getHomeDirectory());
		if(sourceType.equals(SourceTypes.FolderSource))
			chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		
		int returnVal = chooser.showOpenDialog(parent);
		if(returnVal == JFileChooser.APPROVE_OPTION){
			Source source = new Source();
			source.setType(sourceType);
			source.setPath(chooser.getSelectedFile().toString());
			dbIn.addSource(source);
			runCrawler(chooser.getSelectedFile().toString(), dbIn, source);
		}
		
	}
	
	// Basic check to ensure a given audio file is still present in the filesystem
	public static boolean fileStillValid(String filepath, MediaItem fileInfo, DatabaseManager db) throws SQLException {
		File file = new File(filepath);
		if(file.exists() && file.isFile()){
			InputStream fileReader = null;
			try{
				fileReader = new FileInputStream(file);
			}
			catch(FileNotFoundException e1){
				// File should exist, but apparently doesn't
				e1.printStackTrace();
				return false;
			}
			CRC32 crcMaker = new CRC32();
			byte[] buffer = new byte[1024];
			int bytesRead;
			try{
				while((bytesRead = fileReader.read(buffer)) != -1){
					crcMaker.update(buffer, 0, bytesRead);
				}
			}
			catch(IOException e1){
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try{
				fileReader.close();
			}
			catch(IOException e){
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(crcMaker.getValue() != fileInfo.getHash()){
				// File still exists, but has changed somehow
				
				List<edu.purdue.cs408.team9.database.models.Tag> tags = getTagsFromMediaItem(fileInfo);
				try{
					db.getDataCreator().removeMediaItem(fileInfo);
				}
				catch(SQLException e2){
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
				
				AudioFile f = null;
				try{
					f = AudioFileIO.read(file);
				}
				catch(CannotReadException | IOException | TagException | ReadOnlyFileException
									| InvalidAudioFrameException e){
					LogUtils.logError("Audio file exists but cannot be read!");
					e.printStackTrace();
					return false;
				}
				if(f == null){
					// TODO: Throw Error (Should never be reached)
					return false;
				}
				Tag tag = f.getTag();
				
				LogUtils.logInfo("Getting tag info for " + filepath);
				
				fileInfo.setMetaAlbum(tag.getFirst(FieldKey.ALBUM));
				fileInfo.setMetaArtist(tag.getFirst(FieldKey.ARTIST));
				
				if(!tag.getFirst(FieldKey.TITLE).equals("")){
					fileInfo.setMetaTitle(tag.getFirst(FieldKey.TITLE));
				}
				else{
					fileInfo.setMetaTitle(titleForFilepath(filepath));
				}
				
				fileInfo.setMetaDuration(f.getAudioHeader().getTrackLength());
				try{
					fileInfo.setTrackNumber(Integer.parseInt(tag.getFirst(FieldKey.TRACK)));
				}
				catch(NumberFormatException e1){
					fileInfo.setTrackNumber(-1);
				}
				fileInfo.setPath(filepath);
				for(int i = 0; i < tags.size(); i++){
					tags.set(i, db.getDataFetcher().tagForName(tags.get(i).getName()));
				}
				fileInfo.setTransientTags(tags);// set user tags
				fileInfo.setHash(crcMaker.getValue());
				
				try{
					DataCreator dbIn = db.getDataCreator();
					if(!dbIn.addMediaItem(fileInfo)){
						throw new SQLException("Add Media Item Failed");
					}
				}
				catch(SQLException e){
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return true;
		}
		return false;
	}
	
	private static List<edu.purdue.cs408.team9.database.models.Tag> getTagsFromMediaItem(MediaItem song) {
		Object[] songsongtag = song.getTags().toArray();
		List<edu.purdue.cs408.team9.database.models.Tag> songtags = new ArrayList<edu.purdue.cs408.team9.database.models.Tag>(songsongtag.length);
		for(Object o: songsongtag){
			if(!((MediaItemTag) o).getTag().isSystemTag()){
				songtags.add(((MediaItemTag) o).getTag());
			}
		}
		return songtags;
	}
}
