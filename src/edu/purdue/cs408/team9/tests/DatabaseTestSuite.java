package edu.purdue.cs408.team9.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import edu.purdue.cs408.team9.tests.database.DatabaseManagerTests;

@RunWith(Suite.class)
@Suite.SuiteClasses({
  DatabaseManagerTests.class
})
public class DatabaseTestSuite {
  // placeholder for annotation usage
}
