package edu.purdue.cs408.team9.tests.database;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runners.MethodSorters;

import edu.purdue.cs408.team9.database.DataCreator;
import edu.purdue.cs408.team9.database.DatabaseManager;
import edu.purdue.cs408.team9.database.Query;
import edu.purdue.cs408.team9.database.models.MediaItem;
import edu.purdue.cs408.team9.database.models.Tag;
import edu.purdue.cs408.team9.io.DirectoryUtils;

/**
 * Tests for {@link DataCreator}
 * 
 * @author alecgorge
 */

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DataCreatorTests {
  @Rule
  public ExpectedException thrown = ExpectedException.none();

  static DatabaseManager db = null;
  
  @BeforeClass
  public static void a_setupDatabase() throws SQLException, IOException {
    File dbFile = new File(DirectoryUtils.getUserConfigurationDirectory() + "test_db.sqlite");
    System.out.println(dbFile);
    if(dbFile.exists()) {
      dbFile.delete();
    }
    
    db = new DatabaseManager(dbFile);
  }
  
  @Test
  public void b_createMediaItem() throws SQLException {
    MediaItem m = new MediaItem();
    m.setHash(98521456);
    m.setMetaAlbum("ALBUM2");
    m.setMetaArtist("ARITST2");
    m.setMetaDuration(12345);
    m.setMetaTitle("TITLE2");
    m.setPath("/Users/alecgorge/Music/blah2.mp3");
    m.setTrackNumber(1);
    m.setSource(null);
    
    db.getDataCreator().addMediaItem(m);
  }
  
  @Test
  public void c_createMediaItemWithInitialTags() throws SQLException {
    MediaItem m = new MediaItem();
    m.setHash(12345678);
    m.setMetaAlbum("ALBUM");
    m.setMetaArtist("ARITST");
    m.setMetaDuration(1234);
    m.setMetaTitle("TITLE");
    m.setPath("/Users/alecgorge/Music/blah.mp3");
    m.setTrackNumber(4);
    m.setSource(null);
    
    Tag t = new Tag();
    t.setName("happy");
    t.setSystemTag(false);
    
    m.getTransientTags().add(t);
    
    db.getDataCreator().addMediaItemIfNotExistsInDatabase(m);
    
    // load the tags into tags instead of transient tags
    db.getDataFetcher().refreshMediaItem(m);
    
    Assert.assertEquals(3, m.getTags().size());
    
    return;
  }
  
  @Test
  public void d_querySingleTag() throws SQLException {
    Query q = new Query();

    q.addTerm(db.getDataFetcher().tagForName("happy"));
    
    List<MediaItem> items = db.getDataFetcher().getMediaItemsForQuery(q);
    
    Assert.assertEquals(1, items.size());
    Assert.assertEquals("12345678", items.get(0).getHash());
  }
  
  @AfterClass
  public static void zzz_closeDatabase() throws SQLException {
    if(db != null) {
      db.close();      
    }
  }
}
