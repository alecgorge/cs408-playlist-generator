package edu.purdue.cs408.team9.tests.database;

import org.junit.Rule;
import org.junit.rules.ExpectedException;

import edu.purdue.cs408.team9.database.DataFetcher;

/**
 * Tests for {@link DataFetcher}
 * 
 * @author alecgorge
 */
public class DataFetcherTests {
  @Rule
  public ExpectedException thrown = ExpectedException.none();
}
