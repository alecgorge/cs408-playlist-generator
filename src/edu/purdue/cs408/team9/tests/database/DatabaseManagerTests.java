package edu.purdue.cs408.team9.tests.database;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import edu.purdue.cs408.team9.database.DatabaseManager;
import edu.purdue.cs408.team9.io.DirectoryUtils;

/**
 * Tests for {@link DatabaseManager}
 * 
 * @author alecgorge
 */
public class DatabaseManagerTests {
  @Rule
  public ExpectedException thrown= ExpectedException.none();
  
  @Test
  public void testConnectingAndCreatingInMemory() throws SQLException, IOException {
      DatabaseManager m = new DatabaseManager(null);
      m.close();
  }
  
  @Test
  public void testConnectingAndCreatingToFile() throws SQLException, IOException {
    File dbFile = new File(DirectoryUtils.getUserConfigurationDirectory() + "test_db.sqlite");
    System.out.println(dbFile);
    if(dbFile.exists()) {
      dbFile.delete();
    }
    
    DatabaseManager m = new DatabaseManager(dbFile);
    m.close();
    dbFile.delete();
  }
  
  @Test
  public void testDaoCreation() throws SQLException, IOException {
    DatabaseManager m = new DatabaseManager(null);
    
    Assert.assertNotNull(m.getDaoMediaItem());
    Assert.assertNotNull(m.getDaoMediaItemTag());
    Assert.assertNotNull(m.getDaoSavedFilter());
    Assert.assertNotNull(m.getDaoSource());
    Assert.assertNotNull(m.getDaoTag());
    
    m.close();
  }
  
  @Test
  public void testDataCreatorAndFetcherCreation() throws SQLException, IOException {
    DatabaseManager m = new DatabaseManager(null);
    
    Assert.assertNotNull(m.getDataCreator());
    Assert.assertNotNull(m.getDataFetcher());
    
    m.close();
  }
}
