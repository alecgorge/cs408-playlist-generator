package edu.purdue.cs408.team9.tests;

import java.io.IOException;
import java.sql.SQLException;

import edu.purdue.cs408.team9.database.DataCreator;
import edu.purdue.cs408.team9.database.DatabaseManager;
import edu.purdue.cs408.team9.database.models.Source;
import edu.purdue.cs408.team9.io.FileCrawler;

public class FileCrawlerTest {

  private static DatabaseManager db;

  public static void main(String[] args) {

    try {
      db = new DatabaseManager(null);
    } catch (SQLException | IOException e) {
      // TODO:Auto-generated catch block
      e.printStackTrace();
    }

    DataCreator dbIn = db.getDataCreator();

    // Redirect String to point to audio file or folder containing (folders containing) audio files
    FileCrawler.runCrawler("C:\\Users\\owner\\Music\\Ace Attorney", dbIn, new Source());

    try {
      FileCrawler.selectStartPoint(null, dbIn, Source.SourceTypes.FileSource);
    } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

}
