
package edu.purdue.cs408.team9.ui.filterpanel;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import edu.purdue.cs408.team9.HashtagPlaylistApp;
import edu.purdue.cs408.team9.database.DatabaseManager;
import edu.purdue.cs408.team9.database.Query;
import edu.purdue.cs408.team9.database.models.MediaItem;
import edu.purdue.cs408.team9.database.models.SavedFilter;
import edu.purdue.cs408.team9.database.models.Tag;
import edu.purdue.cs408.team9.ui.Filter;

/**
 * Manages filters. The bridge between the FilterPanel ui and the database. It updates the FilterPanel and database.
 */
public class FilterManager {
	private DatabaseManager db;
	private HashtagPlaylistApp mainUI;
	// UI Layer storage of ui items
	private DefaultListModel<Filter> fApplied;
	private DefaultListModel<Filter> fSaved;
	private DefaultListModel<Filter> fMetadata;
	private DefaultListModel<Filter> fTags;
	// UI Layer Storage of db items (because there is no way to get a specific saved filter from the db by name)
	private List<SavedFilter> listSaved;
	
	/**
	 * Constructs a FilterManager. On creation, the FilterManager populates the FilterPanel UI with the Filters in the database.
	 * @param fp
	 */
	public FilterManager(DatabaseManager db, HashtagPlaylistApp mainUI) {
		this.db = db;
		this.mainUI = mainUI;
		fApplied = new DefaultListModel<Filter>();
		fMetadata = new DefaultListModel<Filter>();
		fTags = new DefaultListModel<Filter>();
		fSaved = new DefaultListModel<Filter>();
		listSaved = new ArrayList<SavedFilter>();
		reset();
	}
	
	public void reset() {
		fApplied.clear();
		refresh();
	}
	
	public void refresh() {
		try{
			fMetadata.clear();
			fTags.clear();
			fSaved.clear();
			
			Filter[] meta = new Filter[]{new Filter(Filter.METADATA_ALBUM, "Album"), new Filter(Filter.METADATA_ARTIST, "Artist")};
			for(Filter v: meta){
				addFilterSorted(v, fMetadata);
			}
			
			for(Tag v: db.getDataFetcher().getAllUserTags()){
				if(!addFilterSorted(new Filter(Filter.TAG, v.getName()), fTags)){
					mainUI.showErrorDialog(new Exception("Database has more than one user Tag of the same name."));
				}
			}
			
			updateSavedFilterUIStorage();
			for(SavedFilter v: listSaved){
				if(!addFilterSorted(new Filter(Filter.SAVED, v.getName()), fSaved)){
					mainUI.showErrorDialog(new Exception("Database has more than one Saved Filter of the same name."));
				}
			}
		}
		catch(SQLException e){
			mainUI.showErrorDialog(e);
		}
	}
	
	/**
	 * Returns the DefaultListModel for the applied filters panel. You can call toArray() on this to get an array of filters displayed in the panel.
	 * @return
	 */
	public DefaultListModel<Filter> getListModelApplied() {
		return fApplied;
	}
	
	/**
	 * Returns the DefaultListModel for the metadata filters panel. You can call toArray() on this to get an array of filters displayed in the panel.
	 * @return
	 */
	public DefaultListModel<Filter> getListModelMetadata() {
		return fMetadata;
	}
	
	/**
	 * Returns the DefaultListModel for the tags panel. You can call toArray() on this to get an array of filters displayed in the panel.
	 * @return
	 */
	public DefaultListModel<Filter> getListModelTags() {
		return fTags;
	}
	
	/**
	 * Returns the DefaultListModel for the saved filters panel. You can call toArray() on this to get an array of filters displayed in the panel.
	 * @return
	 */
	public DefaultListModel<Filter> getListModelSaved() {
		return fSaved;
	}
	
	public List<Tag> getTags() {
		Object[] tagFilters = fTags.toArray();
		ArrayList<Tag> list = new ArrayList<Tag>(tagFilters.length);
		try{
			for(Object f: tagFilters){
				Tag tag = db.getDataFetcher().tagForName(((Filter) f).getName());
				if(tag != null){
					list.add(tag);
				}
				else{
					mainUI.showErrorDialog(new Exception("tagForName returned null tag, but the tag exists"));
				}
			}
		}
		catch(SQLException e){
			mainUI.showErrorDialog(e);
		}
		return list;
	}
	
	/**
	 * Gets the filtered songs from the database.
	 * @param withSearch - whether or not to filter with the SearchBar text.
	 * @param validateSongs - whether or not to only return songs that exist in the file systen. Removes invalid the songs from the db.
	 */
	public List<MediaItem> getFilteredMediaItems(boolean withSearch, boolean validateSongs) {
		try{
			// setup query
			Query query = new Query();
			query.setTermJoinType(Query.TermJoinType.Union);// join
			if(mainUI.getFilterPanel().getFilteringMode() == FilterPanel_Applied.INTERSECTION){
				query.setTermJoinType(Query.TermJoinType.Intersection);
			}
			for(Filter f: mainUI.getFilterPanel().getAppliedFilters()){// filters
				Tag t;
				boolean sysTag = true;
				if(f.getType() == Filter.TAG){
					sysTag = false;
				}
				Tag.SystemTagType sysTagType = Tag.SystemTagType.AlbumTag;
				if(f.getType() == Filter.METADATA_ARTIST){
					sysTagType = Tag.SystemTagType.ArtistTag;
				}
				t = db.getDataFetcher().tagForDetails(f.getName(), sysTag, sysTagType);
				query.addTerm(t);
			}
			if(withSearch && mainUI.getSearchBar().isSearching()){// search
				query.setStringFilter(mainUI.getSearchBar().getText());
			}
			
			// get songs from db
			List<MediaItem> list = db.getDataFetcher().getMediaItemsForQuery(query);
			if(validateSongs){
				list = mainUI.getSongPanel().validateSongs(list);
			}
			return list;
		}
		catch(SQLException e){
			mainUI.showErrorDialog(e);
		}
		return new ArrayList<MediaItem>();// return empty arraylist
	}
	
	/**
	 * Prompts the user for a name and created a Saved Filter from the filters in the applied filters panel.
	 * @param uORi - FilterPanel_Applied.UNION or FilterPanel_Applied.INTERSECTION
	 */
	public void createSavedFilter(boolean uORi) {
		try{
			// Create ui filter
			String name = JOptionPane.showInputDialog(mainUI, "Enter a name for this Filter:", "Save Filter", JOptionPane.PLAIN_MESSAGE);
			if(name == null){
				return;
			}
			Filter saved = new Filter(Filter.SAVED, name);
			
			// update ui
			boolean added = addFilterSorted(saved, fSaved);
			
			if(added){
				// convert ui filter to db filter
				SavedFilter sf = new SavedFilter();
				sf.setName(saved.getName());// set name
				// set query (subfilters and mode)
				Query query = new Query();
				query.setTermJoinType(Query.TermJoinType.Union);
				if(uORi == FilterPanel_Applied.INTERSECTION){
					query.setTermJoinType(Query.TermJoinType.Intersection);
				}
				for(Object o: fApplied.toArray()){
					Filter f = (Filter) o;
					Tag t = new Tag();
					t.setName(f.getName());
					if(f.getType() == Filter.TAG){
						t.setSystemTag(false);
					}
					else{
						t.setSystemTag(true);
					}
					if(f.getType() == Filter.METADATA_ALBUM){
						t.setSystemTagType(Tag.SystemTagType.AlbumTag);
					}
					else if(f.getType() == Filter.METADATA_ARTIST){
						t.setSystemTagType(Tag.SystemTagType.ArtistTag);
					}
					query.addTerm(t);
				}
				sf.setQuery(query);
				
				// update db
				if(!db.getDataCreator().addSavedFilter(sf)){
					mainUI.showErrorDialog(new Exception("Create saved filter failed. (db function returned false)"));
				}
				
				// update ui layer storage
				updateSavedFilterUIStorage();
				return;
			}
			else{
				int overwrite = JOptionPane.NO_OPTION;
				overwrite = JOptionPane.showConfirmDialog(mainUI, "Saved Filter \"" + name + "\" already exists. Overwrite Saved Filter", "Overwrite Saved Filter?", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
				if(overwrite == JOptionPane.YES_OPTION){
					// convert ui filter to db version
					SavedFilter sf = getSavedFilterFromUIStorage(name);
					// set query (subfilters and mode)
					Query query = new Query();
					query.setTermJoinType(Query.TermJoinType.Union);
					if(uORi == FilterPanel_Applied.INTERSECTION){
						query.setTermJoinType(Query.TermJoinType.Intersection);
					}
					for(Object o: fApplied.toArray()){
						Filter f = (Filter) o;
						Tag t = new Tag();
						t.setName(f.getName());
						t.setSystemTag(false);
						query.addTerm(t);
					}
					sf.setQuery(query);
					
					// update db
					if(!db.getDataCreator().updateSavedFilter(sf)){
						mainUI.showErrorDialog(new Exception("Update saved filter failed. (db function returned false)"));
					}
					
					// update ui layer storage
					updateSavedFilterUIStorage();
					return;
				}
			}
		}
		catch(SQLException e){
			mainUI.showErrorDialog(e);
		}
	}
	
	/**
	 * Prompts the user for a name and creates a tag.
	 */
	public void createTag() {
		try{
			String name = JOptionPane.showInputDialog(mainUI, "Enter a name for this Tag:", "Create Tag", JOptionPane.PLAIN_MESSAGE);
			if(name == null){
				return;
			}
			Filter tag = new Filter(Filter.TAG, name);
			
			// update ui
			boolean added = addFilterSorted(tag, fTags);
			if(added){
				// update db
				Tag t = new Tag();
				t.setName(tag.getName());
				t.setSystemTag(false);
				if(!db.getDataCreator().addTagIfNotExistsInDatabase(t)){
					mainUI.showErrorDialog(new Exception("Create tag failed. (db function returned false)"));
				}
				mainUI.getTagPanel().updateData(mainUI.getSongPanel().getSelectedMediaItems(), getTags());
				return;
			}
			else{
				JOptionPane.showMessageDialog(mainUI, "Tag \"" + tag.getName() + "\" already exists.", "Tag already exists", JOptionPane.ERROR_MESSAGE);
			}
		}
		catch(SQLException e){
			mainUI.showErrorDialog(e);
		}
	}
	
	/**
	 * Deletes the specified saved filter.
	 * @param f
	 */
	public void deleteSavedFilter(Filter f) {
		try{
			if(f.getType() != Filter.SAVED){
				return;
			}
			if(JOptionPane.showConfirmDialog(mainUI, "Are you sure you want to delete filter \"" + f.getName() + "\"?\n", "Delete Filter", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE) == JOptionPane.YES_OPTION){
				// update ui
				fSaved.removeElement(f);
				
				// update db
				SavedFilter sf = getSavedFilterFromUIStorage(f.getName());
				if(!db.getDataCreator().removeSavedFilter(sf)){
					mainUI.showErrorDialog(new Exception("Delete saved filter failed. (db function returned false)"));
				}
				
				// update ui layer storage
				updateSavedFilterUIStorage();
			}
		}
		catch(SQLException e){
			mainUI.showErrorDialog(e);
		}
	}
	
	/**
	 * Deletes the specified tag.
	 * @param f
	 */
	public void deleteTag(Filter f) {
		try{
			if(JOptionPane.showConfirmDialog(mainUI, "Are you sure you want to delete tag \"" + f.getName() + "\"?\n(It will be removed from all songs, saved filters, and the applied filters list)", "Delete Tag", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE) == JOptionPane.YES_OPTION){
				// update ui
				fTags.removeElement(f);
				if(fApplied.removeElement(f)){
					updateSongPanel();
				}
				
				// update db
				Tag t = db.getDataFetcher().tagForName(f.getName());
				if(!db.getDataCreator().removeTag(t)){
					mainUI.showErrorDialog(new Exception("Delete tag failed. (db function returned false)"));
				}
				
				// update ui layer storage since tags need to be removed from saved filters too
				updateSavedFilterUIStorage();
				updateSongPanel();
			}
		}
		catch(SQLException e){
			mainUI.showErrorDialog(e);
		}
	}
	
	/**
	 * Applies the filter according to its type.
	 * @param f
	 */
	public void applyFilter(Filter f) {
		if(f.getType() == Filter.SAVED){
			loadSavedFilter(f);
		}
		else if(f.isMetadataFilter()){
			applyMetadataFilter(f);
		}
		else if(f.getType() == Filter.TAG){
			addFilterSorted(f, fApplied);
			updateSongPanel();
		}
	}
	
	/**
	 * Prompts the user to select a sub-type of the metadata filter and applies it.
	 * @param f
	 */
	public void applyMetadataFilter(Filter f) {
		try{
			if(f.isMetadataFilter()){
				String str = Filter.getTypeAsString(f.getType());
				Object[] dbfilters;
				if(f.getType() == Filter.METADATA_ALBUM){
					dbfilters = db.getDataFetcher().getAllAlbumTags().toArray();
				}
				else{
					dbfilters = db.getDataFetcher().getAllArtistTags().toArray();
				}
				
				Filter[] filters = new Filter[dbfilters.length];
				for(int i = 0; i < dbfilters.length; i++){
					String name = ((Tag) dbfilters[i]).getName();
					if(name.equals("")){
						name = " ";
					}
					filters[i] = new Filter(f.getType(), name);
				}
				
				ImageIcon icon = Filter.getTypeAsIcon(f.getType());
				Filter chosenF = null;
				if(filters.length > 0){
					chosenF = (Filter) JOptionPane.showInputDialog(mainUI, "Choose " + str + ":", "Apply Metadata Filter: " + str, JOptionPane.PLAIN_MESSAGE, icon, filters, filters[0]);
				}
				else{
					chosenF = (Filter) JOptionPane.showInputDialog(mainUI, "Choose " + str + ":", "Apply Metadata Filter: " + str, JOptionPane.PLAIN_MESSAGE, icon, filters, null);
				}
				if(chosenF == null){
					return;
				}
				if(chosenF.getName().equals(" ")){
					chosenF = new Filter(chosenF.getType(), "");
				}
				addFilterSorted(chosenF, fApplied);
				updateSongPanel();
			}
		}
		catch(SQLException e){
			mainUI.showErrorDialog(e);
		}
	}
	
	/**
	 * Prompts the user for a name and applies the filters that make up this saved filter.
	 * @param f
	 */
	public void loadSavedFilter(Filter f) {
		if(f.getType() != Filter.SAVED){
			return;
		}
		int cont = JOptionPane.YES_OPTION;
		if(!fApplied.isEmpty()){
			cont = JOptionPane.showConfirmDialog(mainUI, "Loading a saved filter will replace all currently applied filters and the filtering mode.\nContinue?", "Load Saved Filter", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
		}
		if(cont == JOptionPane.YES_OPTION){
			clearAppliedFilters();
			
			// get saved filter
			SavedFilter sf = getSavedFilterFromUIStorage(f.getName());
			
			// apply subfilters of saved filter
			ArrayList<Tag> filters = sf.getQuery().getTerms();
			for(Tag filter: filters){
				int type = Filter.TAG;
				if(filter.isSystemTag()){
					if(filter.getSystemTagType() == Tag.SystemTagType.AlbumTag){
						type = Filter.METADATA_ALBUM;
					}
					if(filter.getSystemTagType() == Tag.SystemTagType.ArtistTag){
						type = Filter.METADATA_ARTIST;
					}
				}
				Filter newF = new Filter(type, filter.getName());
				// if it's a tag check the tag ui list for the filter to maintain the same object
				if(type == Filter.TAG){
					for(Object ft: fTags.toArray()){
						if(newF.compareTo((Filter) ft) == 0){
							newF = (Filter) ft;
						}
					}
				}
				addFilterSorted(newF, fApplied);
			}
			
			// apply filtering mode of saved filter
			boolean mode = FilterPanel_Applied.UNION;
			if(sf.getQuery().getTermJoinType() == Query.TermJoinType.Intersection){
				mode = FilterPanel_Applied.INTERSECTION;
			}
			mainUI.getFilterPanel().setFilteringMode(mode);
			updateSongPanel();
		}
	}
	
	/**
	 * Removes an applied filter.
	 * @param f
	 */
	public void removeAppliedFilter(Filter f) {
		fApplied.removeElement(f);
		updateSongPanel();
	}
	
	/**
	 * Clears all applied filters.
	 */
	public void clearAppliedFilters() {
		fApplied.clear();
		updateSongPanel();
	}
	
	public void updateSongPanel() {
		mainUI.getSongPanel().update();
	}
	
	/**
	 * Adds a filter to one of FilterPanel's sub-panels.
	 * @param f
	 * @param lm
	 * @return success, false if filter already exists
	 */
	private boolean addFilterSorted(Filter f, DefaultListModel<Filter> lm) {
		// sort by type, then name
		for(int i = 0; i < lm.getSize(); i++){
			int c = f.compareTo(lm.get(i));
			if(c == 0){
				return false;// filter already exists
			}
			else if(c > 0){
				lm.add(i, f);
				return true;
			}
		}
		lm.addElement(f);
		return true;
	}
	
	private void updateSavedFilterUIStorage() {
		try{
			listSaved = db.getDataFetcher().getAllSavedFilters();
		}
		catch(SQLException e){
			mainUI.showErrorDialog(e);
		}
	}
	
	private SavedFilter getSavedFilterFromUIStorage(String name) {
		for(SavedFilter v: listSaved){
			if(v.getName().equals(name)){
				return v;
			}
		}
		mainUI.showErrorDialog(new Exception("Error: Saved filter not found in ui layer storage."));
		return null;
	}
}
