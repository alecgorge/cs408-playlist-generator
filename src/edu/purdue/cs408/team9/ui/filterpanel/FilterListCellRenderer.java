package edu.purdue.cs408.team9.ui.filterpanel;

import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JList;

import edu.purdue.cs408.team9.ui.Filter;

/**
 * Used to display a visual representation of a Filter in a JList.
 */
@SuppressWarnings("serial")
public class FilterListCellRenderer extends DefaultListCellRenderer{
	@Override
	public Component getListCellRendererComponent(JList<?> arg0, Object arg1, int arg2, boolean arg3, boolean arg4) {
		JLabel  cell = (JLabel)super.getListCellRendererComponent(arg0, arg1, arg2, arg3, arg4);
		Filter filter = (Filter)arg1;
		cell.setText(filter.getName());
		cell.setIcon(Filter.getTypeAsIcon(filter.getType()));
		return cell;
	}
	
}
