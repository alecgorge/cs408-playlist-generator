package edu.purdue.cs408.team9.ui.filterpanel;

import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.JPopupMenu;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JScrollPane;
import javax.swing.JList;

import edu.purdue.cs408.team9.ui.Filter;

import javax.swing.ListSelectionModel;

/**
 * Sub-panel of FilterPanel that displays the Saved Filters.
 */
@SuppressWarnings("serial")
public class FilterPanel_Saved extends JPanel implements ActionListener, MouseListener{
	private FilterManager fm;
	private JMenuItem popupLoad;
	private JMenuItem popupDelete;
	private JList<Filter> list;
	/**
	 * Create the panel.
	 */
	public FilterPanel_Saved(FilterManager fm) {
		this.fm = fm;
		
		popupLoad = new JMenuItem("Load");
		popupLoad.addActionListener(this);
		popupDelete = new JMenuItem("Delete");
		popupDelete.addActionListener(this);
		
		JLabel lblSavedFilters = new JLabel("Saved Filters");
		lblSavedFilters.setToolTipText("These are saved filters that consist of multiple filters and whether to filter by union or intersection.");
		lblSavedFilters.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblSavedFilters.setHorizontalAlignment(SwingConstants.CENTER);
		
		JScrollPane scrollPane = new JScrollPane();
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addComponent(lblSavedFilters, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 136, Short.MAX_VALUE)
				.addComponent(scrollPane, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 136, Short.MAX_VALUE)
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addComponent(lblSavedFilters)
					.addGap(1)
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 67, Short.MAX_VALUE))
		);
		
		list = new JList<Filter>();
		list.setToolTipText("Right click a filter for more options.");
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.setCellRenderer(new FilterListCellRenderer());
		list.setModel(fm.getListModelSaved());
		list.addMouseListener(this);
		scrollPane.setViewportView(list);
		setLayout(groupLayout);

	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == popupLoad){
			fm.applyFilter(list.getSelectedValue());
		}
		else if(e.getSource() == popupDelete){
			fm.deleteSavedFilter(list.getSelectedValue());
		}
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
		if(e.getSource() == list){
			if(e.isPopupTrigger()){
				popupMenu(e);
			}
		}
	}
	
	@Override
	public void mouseReleased(MouseEvent e) {
		if(e.getSource() == list){
			if(e.isPopupTrigger()){
				popupMenu(e);
			}
		}
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		if(e.getSource() == list){
			if(e.getClickCount() == 2){
				fm.applyFilter(list.getSelectedValue());
			}
		}
	}
	
	@Override
	public void mouseEntered(MouseEvent e) {
	}
	
	@Override
	public void mouseExited(MouseEvent e) {
	}
	
	private void popupMenu(MouseEvent e){
		list.setSelectedIndex(list.locationToIndex(e.getPoint()));
		list.repaint();
		JPopupMenu popup = new JPopupMenu();
		popup.add(popupLoad);
		popup.add(popupDelete);
		popup.show(this, e.getX(), e.getY());
	}
}
