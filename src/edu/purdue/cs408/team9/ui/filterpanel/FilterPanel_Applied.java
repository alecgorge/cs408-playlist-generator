
package edu.purdue.cs408.team9.ui.filterpanel;

import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JPopupMenu;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JList;
import javax.swing.ListSelectionModel;
import javax.swing.JScrollPane;
import javax.swing.ImageIcon;
import javax.swing.JToggleButton;

import edu.purdue.cs408.team9.ui.Filter;

/**
 * Sub-panel of FilterPanel that displayed the applied Filters.
 */
@SuppressWarnings("serial")
public class FilterPanel_Applied extends JPanel implements ActionListener, MouseListener {
	public static final boolean UNION = false;
	public static final boolean INTERSECTION = true;
	
	private FilterManager fm;
	private JButton btnSave;
	private JButton btnClear;
	private JLabel lblAppliedFilters;
	private JList<Filter> list;
	private JToggleButton tglbtnMode;
	private boolean mode;
	
	private JMenuItem popupRemove;
	
	/**
	 * Create the panel.
	 */
	public FilterPanel_Applied(FilterManager fm) {
		this.fm = fm;
		
		popupRemove = new JMenuItem("Remove");
		popupRemove.addActionListener(this);
		
		btnSave = new JButton("");
		btnSave.setFocusable(false);
		btnSave.setToolTipText("Save applied filters as a single filter. The filtering mode (union/intersection) is also saved.");
		btnSave.addActionListener(this);
		btnSave.setIcon(new ImageIcon(FilterPanel_Applied.class.getResource("/com/sun/java/swing/plaf/windows/icons/FloppyDrive.gif")));
		
		btnClear = new JButton("Clear");
		btnClear.setFocusable(false);
		btnClear.setToolTipText("Clear the list of applied filters.");
		btnClear.addActionListener(this);
		
		lblAppliedFilters = new JLabel("Applied Filters");
		lblAppliedFilters.setToolTipText("The songs are filtered by the filters listed below.");
		lblAppliedFilters.setHorizontalAlignment(SwingConstants.CENTER);
		lblAppliedFilters.setFont(new Font("Tahoma", Font.PLAIN, 16));
		
		JScrollPane scrollPane = new JScrollPane();
		
		tglbtnMode = new JToggleButton("");
		tglbtnMode.setFocusable(false);
		tglbtnMode.setToolTipText("Filter songs by union or intersection of applied filters. \r\nUnion = Show songs that have any of the applied filters. (#Happy or #Sad or both) \r\nIntersection = Show only songs that have all of the applied filters. (#Happy and #Sad)");
		tglbtnMode.addActionListener(this);
		tglbtnMode.setIcon(new ImageIcon(FilterPanel_Applied.class.getResource("/com/sun/javafx/scene/web/skin/Underline_16x16_JFX.png")));
		tglbtnMode.setSelectedIcon(new ImageIcon(FilterPanel_Applied.class.getResource("/com/sun/javafx/scene/web/skin/Italic_16x16_JFX.png")));
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
							groupLayout.createParallelGroup(Alignment.LEADING)
												.addGroup(groupLayout.createSequentialGroup()
																	.addComponent(tglbtnMode, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
																	.addGap(1)
																	.addComponent(lblAppliedFilters, GroupLayout.DEFAULT_SIZE, 108, Short.MAX_VALUE)
																	.addGap(1)
																	.addComponent(btnSave, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
												.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 158, Short.MAX_VALUE)
												.addGroup(groupLayout.createSequentialGroup()
																	.addComponent(btnClear)
																	.addContainerGap(101, Short.MAX_VALUE))
							);
		groupLayout.setVerticalGroup(
							groupLayout.createParallelGroup(Alignment.LEADING)
												.addGroup(groupLayout.createSequentialGroup()
																	.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE, false)
																						.addComponent(btnSave, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
																						.addComponent(lblAppliedFilters, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
																						.addComponent(tglbtnMode, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
																	.addGap(1)
																	.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 103, Short.MAX_VALUE)
																	.addGap(1)
																	.addComponent(btnClear, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
							);
		mode = tglbtnMode.isSelected();
		
		list = new JList<Filter>();
		list.setToolTipText("Right click a filter for more options.");
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.setCellRenderer(new FilterListCellRenderer());
		list.setModel(fm.getListModelApplied());
		list.addMouseListener(this);
		scrollPane.setViewportView(list);
		setLayout(groupLayout);
		
	}
	
	/**
	 * Gets the filtering mode.
	 */
	public boolean getMode(){
		return mode;
	}
	
	/**
	 * Sets the filtering mode.
	 * @param mode - FilterPanel_Applied.UNION or FilterPanel_Applied.INTERSECTION
	 */
	public void setMode(boolean mode){
		this.mode = mode;
		tglbtnMode.setSelected(mode);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == btnClear){
			fm.clearAppliedFilters();
		}
		else if(e.getSource() == tglbtnMode){
			mode = tglbtnMode.isSelected();
			fm.updateSongPanel();
		}
		else if(e.getSource() == btnSave){
			fm.createSavedFilter(mode);
		}
		else if(e.getSource() == popupRemove){
			fm.removeAppliedFilter(list.getSelectedValue());
		}
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
		if(e.getSource() == list){
			if(e.isPopupTrigger()){
				popupMenu(e);
			}
		}
	}
	
	@Override
	public void mouseReleased(MouseEvent e) {
		if(e.getSource() == list){
			if(e.isPopupTrigger()){
				popupMenu(e);
			}
		}
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		if(e.getSource() == list){
			if(e.getClickCount() == 2){
				fm.removeAppliedFilter(list.getSelectedValue());
			}
		}
	}
	
	@Override
	public void mouseEntered(MouseEvent e) {
	}
	
	@Override
	public void mouseExited(MouseEvent e) {
	}
	
	private void popupMenu(MouseEvent e){
		list.setSelectedIndex(list.locationToIndex(e.getPoint()));
		list.repaint();
		JPopupMenu popup = new JPopupMenu();
		popup.add(popupRemove);
		popup.show(this, e.getX(), e.getY());
	}
}
