
package edu.purdue.cs408.team9.ui.filterpanel;

import javax.swing.JPanel;

import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.border.EmptyBorder;

import edu.purdue.cs408.team9.HashtagPlaylistApp;
import edu.purdue.cs408.team9.database.DatabaseManager;
import edu.purdue.cs408.team9.database.models.MediaItem;
import edu.purdue.cs408.team9.database.models.Tag;
import edu.purdue.cs408.team9.ui.Filter;

/**
 * This is the panel on the left side that contains the filter UI components. 
 */
@SuppressWarnings("serial")
public class FilterPanel extends JPanel {
	private FilterManager fm;
	private FilterPanel_Applied filterPanel_Applied;
	private FilterPanel_Metadata filterPanel_Metadata;
	private FilterPanel_Tags filterPanel_Tags;
	private FilterPanel_Saved filterPanel_Saved;
	
	public FilterPanel(DatabaseManager db, HashtagPlaylistApp mainUI) {
		fm = new FilterManager(db, mainUI);
		
		filterPanel_Applied = new FilterPanel_Applied(fm);
		
		filterPanel_Metadata = new FilterPanel_Metadata(fm);
		filterPanel_Metadata.setBorder(new EmptyBorder(5, 0, 0, 0));
		
		filterPanel_Tags = new FilterPanel_Tags(fm);
		filterPanel_Tags.setBorder(new EmptyBorder(5, 0, 0, 0));
		
		filterPanel_Saved = new FilterPanel_Saved(fm);
		filterPanel_Saved.setBorder(new EmptyBorder(5, 0, 0, 0));
		setLayout(new GridLayout(0, 1, 0, 0));
		add(filterPanel_Applied);
		add(filterPanel_Metadata);
		add(filterPanel_Tags);
		add(filterPanel_Saved);
	}
	
	public void refresh(){
		fm.refresh();
	}
	
	/**
	 * Sets the filtering mode.
	 * @param mode - FilterPanel_Applied.UNION or FilterPanel_Applied.INTERSECTION
	 */
	public void setFilteringMode(boolean mode) {
		filterPanel_Applied.setMode(mode);
	}
	
	/**
	 * Gets the filtering mode.
	 */
	public boolean getFilteringMode() {
		return filterPanel_Applied.getMode();
	}
	
	/**
	 * Gets the filtered songs from the database.
	 * @param withSearch - whether or not to filter with the SearchBar text.
	 * @param validateSongs - whether or not to only return songs that exist in the file system Removes invalid the songs from the db.
	 */
	public List<MediaItem> getFilteredMediaItems(boolean withSearch, boolean validateSongs) {
		return fm.getFilteredMediaItems(withSearch, validateSongs);
	}
	
	/**
	 * Returns an array of the applied filters.
	 * @return
	 */
	public List<Filter> getAppliedFilters() {
		Object[] o = fm.getListModelApplied().toArray();
		ArrayList<Filter> f = new ArrayList<Filter>(o.length);
		for(Object filter:o){
			f.add((Filter)filter);
		}
		return f;
	}
	
	/**
	 * Returns a list of Tags as displayed on the UI.
	 * @return
	 */
	public List<Tag> getTags() {
		return fm.getTags();
	}
	
	public void reset(){
		fm.reset();
	}
}
