
package edu.purdue.cs408.team9.ui.filterpanel;

import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.JPopupMenu;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.SwingConstants;
import javax.swing.JScrollPane;
import javax.swing.JList;

import edu.purdue.cs408.team9.ui.Filter;

import javax.swing.ListSelectionModel;

/**
 * Sub-panel of FilterPanel that displays the Metadata Filter types.
 */
@SuppressWarnings("serial")
public class FilterPanel_Metadata extends JPanel implements ActionListener, MouseListener {
	private FilterManager fm;
	private JMenuItem popupApply;
	private JList<Filter> list;
	
	/**
	 * Create the panel.
	 */
	public FilterPanel_Metadata(FilterManager fm) {
		this.fm = fm;
		
		popupApply = new JMenuItem("Apply");
		popupApply.addActionListener(this);
		
		JLabel lblMetadataFilters = new JLabel("Metadata Filters");
		lblMetadataFilters.setToolTipText("This type of filter corresponds to the file data of a song file.");
		lblMetadataFilters.setHorizontalAlignment(SwingConstants.CENTER);
		lblMetadataFilters.setFont(new Font("Tahoma", Font.PLAIN, 16));
		
		JScrollPane scrollPane = new JScrollPane();
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
							groupLayout.createParallelGroup(Alignment.TRAILING)
												.addComponent(lblMetadataFilters, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE)
												.addComponent(scrollPane, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE)
							);
		groupLayout.setVerticalGroup(
							groupLayout.createParallelGroup(Alignment.LEADING)
												.addGroup(groupLayout.createSequentialGroup()
																	.addComponent(lblMetadataFilters)
																	.addGap(1)
																	.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 44, Short.MAX_VALUE))
							);
		
		list = new JList<Filter>();
		list.setToolTipText("Right click a filter for more options.");
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.setCellRenderer(new FilterListCellRenderer());
		list.setModel(fm.getListModelMetadata());
		list.addMouseListener(this);
		scrollPane.setViewportView(list);
		setLayout(groupLayout);
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == popupApply){
			fm.applyFilter(list.getSelectedValue());
		}
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
		if(e.getSource() == list){
			if(e.isPopupTrigger()){
				popupMenu(e);
			}
		}
	}
	
	@Override
	public void mouseReleased(MouseEvent e) {
		if(e.getSource() == list){
			if(e.isPopupTrigger()){
				popupMenu(e);
			}
		}
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		if(e.getSource() == list){
			if(e.getClickCount() == 2){
				fm.applyFilter(list.getSelectedValue());
			}
		}
	}
	
	@Override
	public void mouseEntered(MouseEvent e) {
	}
	
	@Override
	public void mouseExited(MouseEvent e) {
	}
	
	private void popupMenu(MouseEvent e) {
		list.setSelectedIndex(list.locationToIndex(e.getPoint()));
		list.repaint();
		JPopupMenu popup = new JPopupMenu();
		popup.add(popupApply);
		popup.show(this, e.getX(), e.getY());
	}
	
}
