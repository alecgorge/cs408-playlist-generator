package edu.purdue.cs408.team9.ui.filterpanel;

import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import java.awt.Font;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.ImageIcon;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.Box;

import edu.purdue.cs408.team9.ui.Filter;

import javax.swing.ListSelectionModel;

/**
 * Sub-panel of FilterPanel that displays the tags.
 */
@SuppressWarnings("serial")
public class FilterPanel_Tags extends JPanel implements ActionListener, MouseListener {
	private FilterManager fm;
	private JList<Filter> list;
	private JButton btnCreate;
	
	private JMenuItem popupApply;
	private JMenuItem popupDelete;

	/**
	 * Create the panel.
	 */
	public FilterPanel_Tags(FilterManager fm) {
		this.fm = fm;
		
		popupApply = new JMenuItem("Apply");
		popupApply.addActionListener(this);
		popupDelete = new JMenuItem("Delete");
		popupDelete.addActionListener(this);
		
		btnCreate = new JButton("");
		btnCreate.setFocusable(false);
		btnCreate.setToolTipText("Create Tag");
		btnCreate.setIcon(new ImageIcon(FilterPanel_Tags.class.getResource("/icons/Plus.png")));
		btnCreate.addActionListener(this);
		
		JLabel lblTags = new JLabel("Tags");
		lblTags.setToolTipText("Tags can be added to individual songs to categorize them.");
		lblTags.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblTags.setHorizontalAlignment(SwingConstants.CENTER);
		
		JScrollPane scrollPane = new JScrollPane();
		
		Box horizontalBox = Box.createHorizontalBox();
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(1)
					.addComponent(horizontalBox, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
					.addGap(1)
					.addComponent(lblTags, GroupLayout.DEFAULT_SIZE, 82, Short.MAX_VALUE)
					.addGap(1)
					.addComponent(btnCreate, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
				.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 133, Short.MAX_VALUE)
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING, false)
						.addComponent(lblTags, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
						.addComponent(horizontalBox, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnCreate, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
					.addGap(1)
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 63, Short.MAX_VALUE))
		);
		
		list = new JList<Filter>();
		list.setToolTipText("Right click a filter for more options.");
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.setCellRenderer(new FilterListCellRenderer());
		list.setModel(fm.getListModelTags());
		list.addMouseListener(this);
		scrollPane.setViewportView(list);
		setLayout(groupLayout);

	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == btnCreate){
			fm.createTag();
		}
		else if(e.getSource() == popupApply){
			fm.applyFilter(list.getSelectedValue());
		}
		else if(e.getSource() == popupDelete){
			fm.deleteTag(list.getSelectedValue());
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		if(e.getSource() == list){
			if(e.isPopupTrigger()){
				popupMenu(e);
			}
		}
	}
	
	@Override
	public void mouseReleased(MouseEvent e) {
		if(e.getSource() == list){
			if(e.isPopupTrigger()){
				popupMenu(e);
			}
		}
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		if(e.getSource() == list){
			if(e.getClickCount() == 2){
				fm.applyFilter(list.getSelectedValue());
			}
		}
	}
	
	@Override
	public void mouseEntered(MouseEvent e) {
	}
	
	@Override
	public void mouseExited(MouseEvent e) {
	}
	
	private void popupMenu(MouseEvent e){
		list.setSelectedIndex(list.locationToIndex(e.getPoint()));
		list.repaint();
		JPopupMenu popup = new JPopupMenu();
		popup.add(popupApply);
		popup.add(popupDelete);
		popup.show(this, e.getX(), e.getY());
	}
}
