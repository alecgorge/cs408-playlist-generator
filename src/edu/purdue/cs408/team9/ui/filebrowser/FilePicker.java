package edu.purdue.cs408.team9.ui.filebrowser;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.function.Consumer;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import net.miginfocom.swing.MigLayout;
import edu.purdue.cs408.team9.io.DirectoryUtils;
import edu.purdue.cs408.team9.utils.LogUtils;
import edu.purdue.cs408.team9.utils.SwingUtils;

public class FilePicker extends JPanel {

  private static final long serialVersionUID = 2820053473998771298L;
  private static String browseFileButtonText = "Browse";
  private static String labelText = "Location:";

  private JLabel label;
  private JButton browseButton;
  private JTextField fileSelectionField;
  private String directory;
  private File selectedFile;
  private JFileChooser fileChooser;
  private Consumer<File> callback;

  public FilePicker() {
    this(null);
  }

  public FilePicker(String directory) {
    super();
    if (directory == null) {
      Date date = new Date();
      DateFormat format = new SimpleDateFormat("MM-dd-hh-mm");
      directory =
          DirectoryUtils.getHomeDirectory() + File.separator + "playlist-" + format.format(date)
              + ".pls";
    }
    this.directory = directory;
    selectedFile = new File(directory);
    fileChooser = new JFileChooser(directory);
    fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
    this.setBackground(Color.white);
    this.setLayout(new MigLayout("fillx"));
    label = new JLabel(labelText);
    fileSelectionField = new JTextField();
    fileSelectionField.setEditable(false);
    browseButton = new JButton(browseFileButtonText);
    browseButton.addActionListener(event -> openFileSelector(event));
    this.add(label, "gapleft 10, pad 5, wrap");
    this.add(fileSelectionField, "gapleft 10, gapright 5, width 20:100%, growx");
    this.add(browseButton, "gapright 10, right");
    setPath(selectedFile);
  }

  public String getSelectedFile() {
    String path = DirectoryUtils.getHomeDirectory() + File.separator + System.currentTimeMillis();
    try {
      path = selectedFile.getCanonicalPath();
    } catch (IOException e) {
      LogUtils.logError(e);
    }
    return path;
  }

  public void addFileExtension(String extension) {
    String name = getSelectedFile();
    if (!name.endsWith(extension)) {
      int index = name.lastIndexOf('.');
      if (index > 0 && index > name.lastIndexOf(File.separator)) {
        // existing extension
        name = name.substring(0, index);
      }
      name = name.concat(extension);
    }
    selectedFile = new File(name);
    setPath(selectedFile);
  }

  public void setOnFileSelectedCallback(Consumer<File> callback) {
    this.callback = callback;
  }

  private void openFileSelector(ActionEvent e) {
    int choice = fileChooser.showSaveDialog(this);
    if (choice == JFileChooser.APPROVE_OPTION) {
      selectedFile = fileChooser.getSelectedFile();
      setPath(selectedFile);
      if (callback != null) {
        callback.accept(selectedFile);
      }
    }
  }

  private void setPath(File file) {
    try {
      fileSelectionField.setText(selectedFile.getCanonicalPath());
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  public static void main(String[] args) {

    FilePicker browser = new FilePicker(null);
    JFrame frame = SwingUtils.createTestingFrame(browser);
    frame.setVisible(true);
    frame.setSize(400, 400);


  }

}
