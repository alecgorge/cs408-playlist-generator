
package edu.purdue.cs408.team9.ui;

import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JLabel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

import edu.purdue.cs408.team9.HashtagPlaylistApp;

/**
 * SearchBar contains the TextField and Search Button.
 */
@SuppressWarnings("serial")
public class SearchBar extends JPanel implements MouseListener, ActionListener, KeyListener {
	private HashtagPlaylistApp mainUI;
	private static final String ICON_SEARCH = "/icons/Search.png";
	private static final String ICON_SEARCH_PRESSED = "/icons/Search Pressed.png";
	private static final String ICON_CLEAR = "/icons/Clear.png";
	private static final String ICON_CLEAR_PRESSED = "/icons/Clear Pressed.png";
	private JTextField textField;
	private JLabel lblSearch;
	private boolean searching = false;
	
	/**
	 * Create the panel.
	 */
	public SearchBar(HashtagPlaylistApp mainUI) {
		this.mainUI = mainUI;
		setOpaque(false);
		
		textField = new JTextField();
		textField.setToolTipText("Search for songs with specific text in their Title, Artist, or Album.");
		textField.setColumns(10);
		textField.addActionListener(this);
		textField.addMouseListener(this);
		textField.addKeyListener(this);
		
		lblSearch = new JLabel("");
		lblSearch.setToolTipText("Start/Clear Search");
		lblSearch.setIcon(new ImageIcon(getClass().getResource(ICON_SEARCH)));
		lblSearch.addMouseListener(this);
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
							groupLayout.createParallelGroup(Alignment.TRAILING)
												.addGroup(groupLayout.createSequentialGroup()
																	.addComponent(textField, GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
																	.addPreferredGap(ComponentPlacement.RELATED)
																	.addComponent(lblSearch, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
							);
		groupLayout.setVerticalGroup(
							groupLayout.createParallelGroup(Alignment.LEADING)
												.addComponent(textField, GroupLayout.DEFAULT_SIZE, 25, Short.MAX_VALUE)
												.addComponent(lblSearch, GroupLayout.DEFAULT_SIZE, 25, Short.MAX_VALUE)
							);
		setLayout(groupLayout);
		
	}
	
	/**
	 * Returns whether the user is searching.
	 * @return
	 */
	public boolean isSearching() {
		return searching;
	}
	
	/**
	 * Returns the text in the TextField.
	 * @return
	 */
	public String getText() {
		return textField.getText();
	}
	
	/**
	 * Starts a search.
	 */
	private void startSearch() {
		searching = true;
		lblSearch.setIcon(new ImageIcon(getClass().getResource(ICON_CLEAR)));
		mainUI.getSongPanel().update();
	}
	
	/**
	 * Ends a search.
	 */
	private void endSearch() {
		searching = false;
		lblSearch.setIcon(new ImageIcon(getClass().getResource(ICON_SEARCH)));
		mainUI.getSongPanel().update();
	}
	
	@Override
	public void mousePressed(MouseEvent arg0) {
		if(arg0.getSource() == lblSearch){
			// aesthetic appeal of interactive button
			if(searching){
				lblSearch.setIcon(new ImageIcon(getClass().getResource(ICON_CLEAR_PRESSED)));
			}
			else{
				lblSearch.setIcon(new ImageIcon(getClass().getResource(ICON_SEARCH_PRESSED)));
			}
		}
	}
	
	@Override
	public void mouseReleased(MouseEvent arg0) {
		if(arg0.getSource() == lblSearch){
			if(searching){
				//end search when the end search button is pressed
				endSearch();
				textField.setText("");
			}
			else{
				//start search when the search button is pressed
				startSearch();
			}
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(arg0.getSource() == textField){
			//start search when enter key is pressed
			startSearch();
		}
	}
	
	@Override
	public void keyPressed(KeyEvent arg0) {
		if(arg0.getSource() == textField){
			//end search when the user types in the text field
			if(searching && arg0.getKeyCode() != KeyEvent.VK_ENTER){
				endSearch();
			}
		}
		
	}
	@Override
	public void mouseClicked(MouseEvent arg0) {
	}
	
	@Override
	public void mouseEntered(MouseEvent arg0) {
	}
	
	@Override
	public void mouseExited(MouseEvent arg0) {
	}
	
	
	@Override
	public void keyReleased(KeyEvent arg0) {
	}
	
	@Override
	public void keyTyped(KeyEvent arg0) {
	}
}
