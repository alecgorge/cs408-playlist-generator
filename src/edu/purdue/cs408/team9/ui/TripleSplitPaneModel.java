
package edu.purdue.cs408.team9.ui;

import org.jdesktop.swingx.MultiSplitLayout.Leaf;
import org.jdesktop.swingx.MultiSplitLayout.Split;
import org.jdesktop.swingx.MultiSplitLayout.Divider;

public class TripleSplitPaneModel extends Split
{
	// 3 possible positions
	public static final String P1 = "1";
	public static final String P2 = "2";
	public static final String P3 = "3";
	
	public TripleSplitPaneModel()
	{
		setRowLayout(true);
		Leaf p1 = new Leaf(P1);
		Leaf p2 = new Leaf(P2);
		Leaf p3 = new Leaf(P3);
		
		p1.setWeight(0);
		p2.setWeight(1);
		p3.setWeight(0);
		
		setChildren(p1, new Divider(), p2, new Divider(), p3);
	}
}
