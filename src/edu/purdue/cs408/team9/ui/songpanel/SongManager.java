
package edu.purdue.cs408.team9.ui.songpanel;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.SwingWorker;

import edu.purdue.cs408.team9.HashtagPlaylistApp;
import edu.purdue.cs408.team9.database.DatabaseManager;
import edu.purdue.cs408.team9.database.models.MediaItem;
import edu.purdue.cs408.team9.io.FileCrawler;

/**
 * Manages songs. The bridge between the SongPanel ui and the database. It updates the SongPanel and
 * database.
 */
public class SongManager {
	private DatabaseManager db;
	private HashtagPlaylistApp mainUI;
	private SongTableModel songList;
	
	/**
	 * Constructs the manager and updates the SongTable.
	 * 
	 * @param db
	 * @param mainUI
	 */
	public SongManager(DatabaseManager db, HashtagPlaylistApp mainUI) {
		this.db = db;
		this.mainUI = mainUI;
		
		songList = new SongTableModel(mainUI);
		
		updateSongTable();
	}
	
	/**
	 * Returns the SongTableModel for the SongTable. You can call toArray() on this to get an array of
	 * songs displayed in the table.
	 * 
	 * @return
	 */
	public SongTableModel getSongTableModel() {
		return songList;
	}
	
	/**
	 * Updates the SongTable with a new list of songs from the database, filtered by filters and
	 * search bar text.
	 */
	public void updateSongTable() {
		new UpdateSongTableWorker().execute();// THREAD UpdateSongTableWorker
	}
	
	/**
	 * Checks if each song is in the file system. Deletes invalids from the database and returns the
	 * list with them removed.
	 * 
	 * @param songs
	 * @return
	 */
	public List<MediaItem> validateSongs(List<MediaItem> songs) {
		// validate songs
		try{
			String msgSongs = "The following songs could not be found on your computer so they have been removed from the database: ";
			List<MediaItem> songsInvalid = new ArrayList<MediaItem>();
			for(MediaItem s: songs){
				if(!FileCrawler.fileStillValid(s.getPath(), s, db)){
					msgSongs += "\n" + s.getMetaTitle() + " - " + s.getMetaArtist() + " - " + s.getMetaAlbum() + " - " + s.getPath();
					songsInvalid.add(s);
					
					if(db.getDataFetcher().doesMediaItemExistInDatabase(s)){
						db.getDataCreator().removeMediaItem(s);
					}
				}
			}
			if(!songsInvalid.isEmpty()){
				// update songpanel
				mainUI.getSongPanel().update();
				// notify user
				JOptionPane.showMessageDialog(mainUI, msgSongs, "Invalid Songs", JOptionPane.ERROR_MESSAGE);
				// update list
				for(MediaItem s: songsInvalid){
					songs.remove(s);
				}
			}
		}
		catch(SQLException e){
			mainUI.showErrorDialog(e);
		}
		return songs;
	}
	
	private class UpdateSongTableWorker extends SwingWorker<Object, Object> {
		@Override
		protected Object doInBackground() throws Exception {
			mainUI.getLoadingService().showLoadingIndicator();
			// get filtered songs from db
			List<MediaItem> list = mainUI.getFilterPanel().getFilteredMediaItems(true, false);
			// update ui
			songList.update(list);
			return null;
		}
		
		@Override
		protected void done() {
			mainUI.getLoadingService().hideLoadingIndicator();
		}
	}
}
