
package edu.purdue.cs408.team9.ui.songpanel;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JTable;

import edu.purdue.cs408.team9.database.models.MediaItem;

/**
 * Displays Songs as rows but with metadata formatted into columns.
 */
@SuppressWarnings("serial")
public class SongTable extends JTable {
	
	private SongTableModel tableModel;
	
	/**
	 * Constructs the table with display data managed by the parameter.
	 * 
	 * @param m - SongTableModel containing the list of songs.
	 */
	public SongTable(SongTableModel m) {
		this.tableModel = m;
		setAutoCreateRowSorter(true);
		setModel(m);
	}
	
	/**
	 * Returns a list of the displayed songs.
	 * 
	 * @return
	 */
	public List<MediaItem> getSongs() {
		return tableModel.getMediaItems();
	}
	
	/**
	 * Returns a list of the selected songs.
	 * 
	 * @return
	 */
	public List<MediaItem> getSelectedSongs() {
		ArrayList<MediaItem> selected = new ArrayList<MediaItem>();
		for(int i: getSelectedRows()){
			selected.add(getSongAt(i));
		}
		return selected;
	}
	
	/**
	 * Returns a song by index on the table. (This index may be different from the index in the
	 * SongTableModel)
	 * 
	 * @param i
	 * @return
	 */
	public MediaItem getSongAt(int i) {
		return (MediaItem) getValueAt(i, -1);
	}
	
	public Object getValueAt(int row, int column) {
		try{
			return super.getValueAt(row, column);
		}
		catch(IndexOutOfBoundsException e){
			return null;// i'm a cheater :P
		}
	}
}
