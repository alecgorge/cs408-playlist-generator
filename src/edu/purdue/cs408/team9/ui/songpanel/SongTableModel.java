
package edu.purdue.cs408.team9.ui.songpanel;

import java.util.List;

import javax.swing.table.DefaultTableModel;

import edu.purdue.cs408.team9.HashtagPlaylistApp;
import edu.purdue.cs408.team9.database.models.MediaItem;

/**
 * Formats a list of songs to be displayed in a table.
 */
@SuppressWarnings("serial")
public class SongTableModel extends DefaultTableModel {
	private static final String[] cols = {"Name", "Artist", "Album", "#", "Length"};
	private List<MediaItem> songs;
	HashtagPlaylistApp mainUI;
	
	/**
	 * Constructs an empty TableModel with predefined columns that refer to song metadata.
	 */
	public SongTableModel(HashtagPlaylistApp mainUI) {
		super(cols, 0);
		this.mainUI = mainUI;
	}
	
	/**
	 * Updates the list of songs to display with a new one.
	 * 
	 * @param songsUpdate
	 */
	public void update(List<MediaItem> songsUpdate) {
		for(int i = 0; i < getRowCount(); i++){
			removeRow(i);
		}
		setRowCount(0);
		songs = songsUpdate;
		if(songs == null){
			return;
		}
		// add song data to rows
		for(MediaItem m: songs){
			addRow(new Object[]{m.getMetaTitle(), m.getMetaArtist(), m.getMetaAlbum(),
								formatNumber(m.getTrackNumber()), formatTime(m.getMetaDuration())});
		}
		mainUI.getSongPanel().updateSongCount();
	}
	
	/**
	 * Returns the song at the row if col < 0. Returns the value displayed in the table cell
	 * otherwise.
	 * 
	 * @param row - should be >= 0
	 * @param col - col < 0 to get a song at the row, col >= 0 to get the value displayed in the table
	 *        cell.
	 */
	public Object getValueAt(int row, int column) {
		if(column < 0){
			return songs.get(row);
		}
		else{
			return super.getValueAt(row, column);
		}
	}
	
	/**
	 * Simply ensures that all cells in the table are unable to be edited.
	 */
	public boolean isCellEditable(int x, int y) {
		return false;
	}
	
	private String formatNumber(int num) {
		String n = "";
		if(num < 0){
			num = 0;
		}
		if(num < 10){
			n += "0";
		}
		if(num < 100){
			n += "0";
		}
		n += num;
		return n;
	}
	
	private String formatTime(long seconds) {
		String time = "";
		long min = seconds / 60;
		long sec = seconds % 60;
		time += min + ":";
		if(sec < 10){
			time += "0";
		}
		time += sec;
		return time;
	}
	
	public List<MediaItem> getMediaItems() {
		return songs;
	}
}
