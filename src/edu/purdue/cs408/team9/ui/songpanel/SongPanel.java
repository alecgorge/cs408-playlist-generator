
package edu.purdue.cs408.team9.ui.songpanel;

import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.SwingWorker;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JScrollPane;

import java.util.ArrayList;
import java.util.List;

import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import edu.purdue.cs408.team9.HashtagPlaylistApp;
import edu.purdue.cs408.team9.database.DatabaseManager;
import edu.purdue.cs408.team9.database.models.MediaItem;
import edu.purdue.cs408.team9.database.models.Tag;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

/**
 * Displays the list of songs filtered or not.
 */
@SuppressWarnings("serial")
public class SongPanel extends JPanel implements ListSelectionListener {
	private SongManager sm;
	private SongTable table;
	private HashtagPlaylistApp mainUI;
	private boolean selectionLock = false;
	private JLabel lblSongsDisplayed;
	private static final String numSongText = " Songs";
	
	/**
	 * Create the panel.
	 */
	public SongPanel(DatabaseManager db, HashtagPlaylistApp mainUI) {
		this.mainUI = mainUI;
		sm = new SongManager(db, mainUI);
		
		JScrollPane scrollPane = new JScrollPane();
		
		lblSongsDisplayed = new JLabel("0 Songs Displayed");
		lblSongsDisplayed.setHorizontalAlignment(SwingConstants.RIGHT);
		lblSongsDisplayed.setHorizontalTextPosition(SwingConstants.CENTER);
		
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
							groupLayout.createParallelGroup(Alignment.LEADING)
												.addComponent(lblSongsDisplayed, GroupLayout.DEFAULT_SIZE, 450, Short.MAX_VALUE)
												.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 450, Short.MAX_VALUE)
							);
		groupLayout.setVerticalGroup(
							groupLayout.createParallelGroup(Alignment.LEADING)
												.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
																	.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 280, Short.MAX_VALUE)
																	.addGap(1)
																	.addComponent(lblSongsDisplayed))
							);
		
		table = new SongTable(sm.getSongTableModel());
		table.setFillsViewportHeight(true);
		table.setShowVerticalLines(false);
		table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		table.getSelectionModel().addListSelectionListener(this);
		
		table.getColumnModel().getColumn(0).setPreferredWidth(147);
		table.getColumnModel().getColumn(1).setPreferredWidth(118);
		table.getColumnModel().getColumn(2).setPreferredWidth(107);
		
		lblSongsDisplayed.setText(table.getRowCount() + numSongText);
		
		scrollPane.setViewportView(table);
		setLayout(groupLayout);
	}
	
	/**
	 * Returns a list of the songs on display as MediaItems.
	 * @return
	 */
	public List<MediaItem> getMediaItems() {
		return table.getSongs();
	}
	
	/**
	 * Returns a list of the selected songs as MediaItems.
	 * @return
	 */
	public List<MediaItem> getSelectedMediaItems() {
		return table.getSelectedSongs();
	}
	
	/**
	 * Updates the SongPanel display.
	 */
	public void update() {
		new UpdateWorker().execute();// THREAD UpdateWorker - updates SongPanel
	}
	
	public void updateSongCount() {
		lblSongsDisplayed.setText(table.getRowCount() + numSongText);
	}
	
	public List<MediaItem> validateSongs(List<MediaItem> songs) {
		return sm.validateSongs(songs);
	}
	
	@Override
	public void valueChanged(ListSelectionEvent arg0) {
		if(!selectionLock){
			selectionLock = true;
			new HandleSelectedSongsWorker(!arg0.getValueIsAdjusting()).execute();// THREAD HandleSelectedSongsWorker
			selectionLock = false;
		}
	}
	
	private class HandleSelectedSongsWorker extends SwingWorker<Object, Object> {
		private boolean loadingIndicatorShown = false;
		private boolean validate;
		
		private HandleSelectedSongsWorker(boolean validate) {
			this.validate = validate;
		}
		
		@Override
		protected Object doInBackground() throws Exception {
			// get selected songs
			List<MediaItem> songs = getSelectedMediaItems();
			if(songs.size() >= 30){
				loadingIndicatorShown = true;
				mainUI.getLoadingService().showLoadingIndicator();
			}
			// make sure that they exist
			if(validate){
				songs = validateSongs(songs);
			}
			// get all tags that exist
			List<Tag> tags = mainUI.getFilterPanel().getTags();
			// update TagPanel
			mainUI.getTagPanel().updateData(songs, tags);
			return null;
		}
		
		@Override
		protected void done() {
			if(loadingIndicatorShown){
				mainUI.getLoadingService().hideLoadingIndicator();
				loadingIndicatorShown = false;
			}
		}
	}
	
	private class UpdateWorker extends SwingWorker<Object, Object> {
		@Override
		protected Object doInBackground() throws Exception {
			mainUI.getLoadingService().showLoadingIndicator();
			table.clearSelection();
			mainUI.getTagPanel().updateData(new ArrayList<MediaItem>(), mainUI.getFilterPanel().getTags());
			sm.updateSongTable();
			return null;
		}
		
		@Override
		protected void done() {
			mainUI.getLoadingService().hideLoadingIndicator();
		}
	}
}
