package edu.purdue.cs408.team9.ui.tagpanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import edu.purdue.cs408.team9.HashtagPlaylistApp;

@SuppressWarnings("serial")
public class PopUpDemo extends JPopupMenu {
	JMenuItem anItem;

	public PopUpDemo(HashtagPlaylistApp mainUI) {
    anItem = new JMenuItem("Remove tag");
    add(anItem);
    anItem.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			TagPanel t = mainUI.getTagPanel();
			try {
				t.doremove();
			} catch (SQLException e1) {
				mainUI.showErrorDialog(e1);
			}
		}
    	});
	}
}