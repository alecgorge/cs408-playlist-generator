
package edu.purdue.cs408.team9.ui.tagpanel;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingWorker;

import edu.purdue.cs408.team9.HashtagPlaylistApp;
import edu.purdue.cs408.team9.database.DataCreator;
import edu.purdue.cs408.team9.database.DatabaseManager;
import edu.purdue.cs408.team9.database.models.MediaItem;
import edu.purdue.cs408.team9.database.models.MediaItemTag;
import edu.purdue.cs408.team9.database.models.Tag;
import edu.purdue.cs408.team9.ui.Filter;

import javax.swing.JLabel;

@SuppressWarnings("serial")
public class TagPanel extends JPanel {
	private JList<Tag> list;
	public DefaultListModel<Tag> model;
	private List<MediaItem> songs;
	private List<Tag> tags;
	private JLabel label;
	private HashtagPlaylistApp mainUI;
	private DatabaseManager db;
	private static final String numSelectedText = " Selected";
	
	public TagPanel(DatabaseManager db, HashtagPlaylistApp mainUI) {
		this.db = db;
		this.mainUI = mainUI;
		this.songs = new ArrayList<MediaItem>();
		this.tags = new ArrayList<Tag>();
		model = new DefaultListModel<Tag>();
		list = new JList<Tag>(model);
		
		JScrollPane pane = new JScrollPane(list);
		JButton addButton = new JButton("Add Tag");
		
		list.addMouseListener(new PopClickListener(mainUI));
		
		addButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Object[] allTags = tags.toArray();
				Object first = null;
				if(allTags.length > 0){
					first = allTags[0];
				}
				Tag selectedTag = (Tag) JOptionPane.showInputDialog(mainUI, "Choose a tag", "Add Tag: ", JOptionPane.PLAIN_MESSAGE, Filter.getTypeAsIcon(Filter.TAG), allTags, first);
				if(selectedTag != null){
					try{
						addTagToSongs(selectedTag);
					}
					catch(SQLException e1){
						mainUI.showErrorDialog(e1);
					}
				}
			}
		});
		setLayout(new BorderLayout(0, 0));
		
		add(pane);
		label = new JLabel("0" + numSelectedText);
		
		add(addButton, BorderLayout.SOUTH);
		add(label, BorderLayout.NORTH);
	}
	
	public void updateData(List<MediaItem> songs, List<Tag> tags) {
		this.songs = songs;
		this.tags = tags;
		showtags();
	}
	
	public void showtags() {
		model.clear();
		label.setText(songs.size() + numSelectedText);
		Iterator<MediaItem> itr = songs.iterator();
		while(itr.hasNext()){
			MediaItem song = itr.next();
			List<Tag> songtags = getTagsFromMediaItem(song);
			Iterator<Tag> tagitr = songtags.iterator();
			while(tagitr.hasNext()){
				Tag atag = tagitr.next();
				if(!model.contains(atag)){
					model.addElement(atag);
				}
			}
		}
	}
	
	public void addTagToSongs(Tag tag) throws SQLException {
		// THREAD AddTagToSongsWorker
		new AddTagToSongsWorker(tag).execute();
	}
	
	public void doremove() throws SQLException {
		if(!model.isEmpty()){
			Tag s = (Tag) list.getSelectedValue();
			if(s != null){
				removeTagFromSongs(s);
			}
		}
	}
	
	public void removeTagFromSongs(Tag tag) throws SQLException {
		// THREAD RemoveTagFromSongsWorker
		new RemoveTagFromSongsWorker(tag).execute();
	}
	
	private void refreshSongPanel() {
		mainUI.getSongPanel().update();
	}
	
	private List<Tag> getTagsFromMediaItem(MediaItem song) {
		Object[] songsongtag = song.getTags().toArray();
		List<Tag> songtags = new ArrayList<Tag>(songsongtag.length);
		for(Object o: songsongtag){
			if(!((MediaItemTag) o).getTag().isSystemTag()){
				songtags.add(((MediaItemTag) o).getTag());
			}
		}
		return songtags;
	}
	
	private class AddTagToSongsWorker extends SwingWorker<Object, Object> {
		Tag tag;
		
		private AddTagToSongsWorker(Tag tag) {
			this.tag = tag;
		}
		
		@Override
		protected Object doInBackground() throws Exception {
			mainUI.getLoadingService().showLoadingIndicator();
			Iterator<MediaItem> itr = songs.iterator();
			while(itr.hasNext()){
				MediaItem song = itr.next();
				List<Tag> songtags = getTagsFromMediaItem(song);
				if(!songtags.contains(tag))// if media item doesn't have the tag..
				{
					if(!db.getDataCreator().addTagToMediaItem(song, tag)){
						mainUI.showErrorDialog(new Exception("Add tag to song didn't work. (db function returned false)"));
					}
					else{
						if(!model.contains(tag)){
							model.addElement(tag);
						}
					}
				}
			}
			refreshSongPanel();
			return null;
		}
		
		@Override
		protected void done() {
			mainUI.getLoadingService().hideLoadingIndicator();
		}
	}
	
	private class RemoveTagFromSongsWorker extends SwingWorker<Object, Object> {
		Tag tag;
		
		private RemoveTagFromSongsWorker(Tag tag) {
			this.tag = tag;
		}
		
		@Override
		protected Object doInBackground() throws Exception {
			mainUI.getLoadingService().showLoadingIndicator();
			Iterator<MediaItem> itr = songs.iterator();
			while(itr.hasNext()){// for each song
				MediaItem song = itr.next();
				// find the tag to remove
				List<Tag> songtags = getTagsFromMediaItem(song);
				Iterator<Tag> tgitr = songtags.iterator();
				Tag removedTag = null;
				while(tgitr.hasNext()){
					Tag temTag = tgitr.next();
					if(temTag.getName().equals(tag.getName())){
						removedTag = temTag;
						break;
					}
				}
				// remove tag if found
				if(removedTag != null){
					DataCreator dc = db.getDataCreator();
					if(!dc.removeTagFromMediaItem(song, removedTag)){
						mainUI.showErrorDialog(new Exception("Remove tag from song didn't work. (db function returned false)"));
					}
					else{
						if(model.contains(tag)){
							model.removeElementAt(model.indexOf(tag));
						}
					}
				}
			}
			refreshSongPanel();
			return null;
		}
		
		@Override
		protected void done() {
			mainUI.getLoadingService().hideLoadingIndicator();
		}
	}
}
