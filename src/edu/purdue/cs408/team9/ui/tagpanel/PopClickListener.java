package edu.purdue.cs408.team9.ui.tagpanel;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import edu.purdue.cs408.team9.HashtagPlaylistApp;

class PopClickListener extends MouseAdapter {
  static PopUpDemo menu;
  private HashtagPlaylistApp mainUI;
  
  public PopClickListener(HashtagPlaylistApp mainUI){
	  this.mainUI=mainUI;
  }

  public void mousePressed(MouseEvent e) {
    if (e.isPopupTrigger())
      doPop(e);
  }

  public void mouseReleased(MouseEvent e) {
    if (e.isPopupTrigger())
      doPop(e);
  }

  public void doPop(MouseEvent e) {
	if(!mainUI.getTagPanel().model.isEmpty())
	{
    menu = new PopUpDemo(mainUI);
    menu.show(e.getComponent(), e.getX(), e.getY());
	}
  }
}
