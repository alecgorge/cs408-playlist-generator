
package edu.purdue.cs408.team9.ui;

import javax.swing.ImageIcon;

/**
 * Ui interpretation of a filter/tag.
 */
public class Filter implements Comparable<Filter> {
	public static final int SAVED = 0;
	public static final int TAG = 1;
	public static final int METADATA_ALBUM = 2;
	public static final int METADATA_ARTIST = 3;
	
	private static final String[] typeString = new String[]{"Saved Filter", "Tag", "Album", "Artist"};
	private static final String[] typeIcon = new String[]{"/com/sun/java/swing/plaf/windows/icons/FloppyDrive.gif", "/icons/Tag.png", "/com/sun/java/swing/plaf/windows/icons/Directory.gif", "/com/sun/javafx/scene/control/skin/modena/HTMLEditor-Background-Color-Black.png"};
	
	private String name;
	private int type;
	
	public Filter(int type, String name) {
		this.name = name;
		this.type = type;
	}
	
	public int getType() {
		return type;
	}
	
	public String getName() {
		return name;
	}
	
	public int compareTo(Filter f) {
		int c = Integer.valueOf(f.getType()).compareTo(type);
		if(c != 0){
			return c;
		}
		c = f.getName().compareTo(name);
		return c;
	}
	
	public boolean isMetadataFilter() {
		return type >= METADATA_ALBUM;
	}
	
	public static String getTypeAsString(int type) {
		return typeString[type];
	}
	
	public static ImageIcon getTypeAsIcon(int type) {
		return new ImageIcon(Filter.class.getResource(typeIcon[type]));
	}
	
	public String toString() {
		return name;
	}
}
