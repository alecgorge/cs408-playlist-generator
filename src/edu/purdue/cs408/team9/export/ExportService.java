package edu.purdue.cs408.team9.export;

import java.awt.Frame;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;

import edu.purdue.cs408.team9.database.models.MediaItem;
import edu.purdue.cs408.team9.exceptions.FileNotWriteableException;
import edu.purdue.cs408.team9.export.formats.BaseFormat;
import edu.purdue.cs408.team9.loading.LoadingService;
import edu.purdue.cs408.team9.ui.filterpanel.FilterPanel;

public class ExportService {

  private FilterPanel filterPanel;

  public ExportService(FilterPanel filterPanel) {
    this.filterPanel = filterPanel;
  }

  public void showDialog(Frame owner, LoadingService loadingService) {
    ExportDialog dialog = new ExportDialog(owner, this, loadingService);
    dialog.setVisible(true);
    dialog.setSize(600, 250);
  }

  public List<MediaItem> getSelectedMediaItems() {
    // Nick: Use filterPanel.getFilteredMediaItems(false, true); to get the MediaItems to be in the
    // playlist.
    if (filterPanel == null)
      return null;
    return filterPanel.getFilteredMediaItems(false, true);
  }

  public void writeToFile(List<MediaItem> items, BaseFormat format, File outputFile)
      throws NullPointerException, FileNotWriteableException, IOException {
    if (outputFile == null || format == null) {
      throw new NullPointerException();
    }
    BufferedWriter writer = null;
    try {
      writer =
          new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile, false),
              "utf-8"));
      writer.write(format.getFileHeader());

      for (int i = 0; i < items.size(); i++) {
        writer.write(format.getSongData(items.get(i), i + 1));
      }

      writer.write(format.getFileFooter(items.size()));

    } catch (IOException e) {
      e.printStackTrace();
      throw e;
    } finally {
      if (writer != null) {
        writer.close();
      }
    }


  }


  public static void main(String[] args) {
    ExportService service = new ExportService(null);
    service.showDialog(null, null);
  }
}
