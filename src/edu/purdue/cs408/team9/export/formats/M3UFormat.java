package edu.purdue.cs408.team9.export.formats;

import edu.purdue.cs408.team9.database.models.MediaItem;

public class M3UFormat extends BaseFormat {

  private static String extension = ".m3u";
  private static String fileHeader = "#EXTM3U\n";
  private static String fileFooter = "\n";

  @Override
  public String getFileHeader() {
    return fileHeader;
  }

  @Override
  public String getSongData(MediaItem mediaItem, int position) {
    // duration should be in seconds
    // #EXTINF:123, Sample artist - Sample title
    // <PATH>
    Long duration = mediaItem.getMetaDuration();
    String artist = mediaItem.getMetaArtist();
    String title = mediaItem.getMetaTitle();
    String path = mediaItem.getPath();
    return String.format("#EXTINF:%d, %s - %s\n%s\n", duration, artist, title, path);
  }

  @Override
  public String getFileFooter(int totalTracks) {
    return fileFooter;
  }

  @Override
  public String getFileExtension() {
    return extension;
  }

  public static void main(String[] args) {
    M3UFormat formatter = new M3UFormat();
    MediaItem item = new MediaItem();
    item.setMetaArtist("Really cool artist");
    item.setMetaDuration(32L);
    item.setMetaTitle("Really cool song");
    item.setPath("/homes/nick/music/song.mp3");
    System.out.println("*********M3U***********");
    System.out.print(formatter.getFileHeader());
    System.out.print(formatter.getSongData(item, 1));
    System.out.print(formatter.getFileFooter(1));
    System.out.println("*********END***********");

  }
}
