package edu.purdue.cs408.team9.export.formats;

import edu.purdue.cs408.team9.database.models.MediaItem;

public abstract class BaseFormat {

  /**
   * Get the file extension required by the format
   * 
   * @return
   */
  public abstract String getFileExtension();

  /**
   * Get the format specific file header
   * 
   * @return
   */
  public abstract String getFileHeader();

  /**
   * Get the format for a given MediaItem
   * 
   * @param mediaItem
   * @param position
   * @return
   */
  public abstract String getSongData(MediaItem mediaItem, int position);

  /**
   * Get the required footer (if any)
   * 
   * @return
   */
  public abstract String getFileFooter(int totalTracks);
}
