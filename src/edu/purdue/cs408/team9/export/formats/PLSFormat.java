package edu.purdue.cs408.team9.export.formats;

import edu.purdue.cs408.team9.database.models.MediaItem;

public class PLSFormat extends BaseFormat {

  private static String extension = ".pls";
  private static String fileHeader = "[playlist]\n";
  private static String fileFooter = "NumberOfEntries=%d\nVersion=2\n";

  @Override
  public String getFileHeader() {
    return fileHeader;
  }

  @Override
  public String getSongData(MediaItem mediaItem, int position) {
    // File1=/Alternative\everclear-SMFTA.mp3
    // Title1=Everclear - So Much For The Afterglow
    // Length1=233

    Long duration = mediaItem.getMetaDuration();
    String title = mediaItem.getMetaTitle();
    String path = mediaItem.getPath();

    StringBuilder builder = new StringBuilder();
    builder.append(String.format("File%d=%s\n", position, path));
    builder.append(String.format("Title%d=%s\n", position, title));
    builder.append(String.format("Length%d=%d\n", position, duration));

    return builder.toString();
  }

  @Override
  public String getFileFooter(int totalTracks) {
    return String.format(fileFooter, totalTracks);
  }

  @Override
  public String getFileExtension() {
    return extension;
  }

  public static void main(String[] args) {
    PLSFormat formatter = new PLSFormat();
    MediaItem item = new MediaItem();
    item.setMetaArtist("Really cool artist");
    item.setMetaDuration(32L);
    item.setMetaTitle("Really cool song");
    item.setPath("/homes/nick/music/song.mp3");
    System.out.println("*********PLS***********");
    System.out.print(formatter.getFileHeader());
    System.out.print(formatter.getSongData(item, 1));
    System.out.print(formatter.getFileFooter(1));
    System.out.println("*********END***********");

  }
}
