package edu.purdue.cs408.team9.export;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import net.miginfocom.swing.MigLayout;
import edu.purdue.cs408.team9.database.models.MediaItem;
import edu.purdue.cs408.team9.export.formats.BaseFormat;
import edu.purdue.cs408.team9.export.formats.M3UFormat;
import edu.purdue.cs408.team9.export.formats.PLSFormat;
import edu.purdue.cs408.team9.loading.LoadingService;
import edu.purdue.cs408.team9.ui.filebrowser.FilePicker;

public class ExportDialog extends JDialog {
  private static final long serialVersionUID = 116674107354169297L;
  private static String title = "Export Playlist";
  private static ModalityType modality = ModalityType.APPLICATION_MODAL;
  private ExportService exportService;
  private LoadingService loadingService;
  private JComboBox<FormatType> formatTypeSelect;
  private FilePicker filePicker;
  private JButton exportPlaylistButton;

  public ExportDialog(Frame owner, ExportService exportService, LoadingService loadingService) {
    super(owner, ExportDialog.title, ExportDialog.modality);
    this.exportService = exportService;
    this.loadingService = loadingService;
    if (loadingService == null) {
      this.loadingService = new LoadingService();
    }
    this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    this.setMinimumSize(new Dimension(600, 250));
    initComponents();
    pack();

  }

  private void initComponents() {
    JLabel formatLabel = new JLabel("Playlist format:");
    formatTypeSelect = new JComboBox<FormatType>(FormatType.values());
    formatTypeSelect.addActionListener(event -> formatChanged(event));
    filePicker = new FilePicker();
    filePicker.setOnFileSelectedCallback(file -> addExtension(file));
    exportPlaylistButton = new JButton("Export");
    exportPlaylistButton.addActionListener(event -> exportPlaylist(event));
    exportPlaylistButton.setFont(new Font("sanserif", Font.BOLD, 18));

    Container contentPane = this.getContentPane();
    contentPane.setBackground(Color.white);
    contentPane.setLayout(new MigLayout("fill"));
    contentPane.add(formatLabel, "gapleft 10");
    contentPane.add(formatTypeSelect, "wrap");
    contentPane.add(filePicker, "span 2, width 100%!, wrap");
    contentPane.add(exportPlaylistButton,
        "gapbottom 10, gapleft 10, gapright 10, width ::200, south, span 2, right");


  }

  private void formatChanged(ActionEvent event) {
    filePicker.addFileExtension(getSelectedFormat().getFileExtension());
  }

  private void addExtension(File file) {
    filePicker.addFileExtension(getSelectedFormat().getFileExtension());
  }

  private BaseFormat getSelectedFormat() {
    FormatType selectedFormat = FormatType.values()[formatTypeSelect.getSelectedIndex()];
    BaseFormat formatter;
    switch (selectedFormat) {
      case PLS:
        formatter = new PLSFormat();
        break;
      case M3U:
        formatter = new M3UFormat();
        break;
      default:
        formatter = new M3UFormat();
        break;
    }
    return formatter;
  }

  private void exportPlaylist(ActionEvent event) {
    if (loadingService != null) {
      loadingService.showLoadingIndicator();
    }
    exportPlaylistButton.setEnabled(false);

    File outputFile = new File(filePicker.getSelectedFile());
    List<MediaItem> items = exportService.getSelectedMediaItems();
    BaseFormat formatter = getSelectedFormat();
    boolean success = false;

    try {
      exportService.writeToFile(items, formatter, outputFile);
      success = true;
    } catch (NullPointerException | IOException e) {
      e.printStackTrace();

    }
    if (loadingService != null) {
      loadingService.hideLoadingIndicator();
    }
    exportPlaylistButton.setEnabled(true);

    if (success) {
      JOptionPane.showMessageDialog(this, "Export Complete");
      this.dispose();
    } else {
      JOptionPane.showMessageDialog(this, "Export error.");
      return;
    }

  }

  public static void main(String[] args) throws Exception {
    new ExportService(null).showDialog(null, null);
  }
}
