package edu.purdue.cs408.team9.utils;

import java.awt.Component;

import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class SwingUtils {

  /**
   * Sometimes it works out better if you throw your component into a panel so that it doesn't
   * resize weirdly, but you probably don't need to keep an reference or constantly keep creating
   * new jpanels
   * 
   * @param component
   * @return
   */
  public static JPanel addToNewPanel(Component component) {
    JPanel panel = new JPanel();
    panel.add(component);
    return panel;
  }

  public static JPanel addToVerticalPanel(Component... components) {
    JPanel panel = new JPanel();
    panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
    SwingUtils.addAllToComponent(panel, components);
    return panel;
  }

  public static JPanel addToHorizontalPanel(Component... components) {
    JPanel panel = new JPanel();
    panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
    SwingUtils.addAllToComponent(panel, components);
    return panel;
  }

  public static void addAllToComponent(JComponent parent, Component... components) {
    if (parent == null)
      return;
    for (Component child : components) {
      parent.add(child);
    }
  }

  public static JFrame createTestingFrame(Component component) {
    JFrame frame = new JFrame("TESTING");
    frame.getContentPane().add(component);
    frame.pack();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    return frame;
  }
}
